#!/usr/bin/env python3

import graphviz

nodes = [
    ('closed', {'label': 'Closed'}),
    ('half_closed', {'label': 'Half-closed'}),
    ('left_to_left', {'label': 'Left to left'}),
    ('left_to_right', {'label': 'Left to right'}),
    ('right_to_left', {'label': 'Right to left'}),
    ('right_to_right', {'label': 'Right to right'}),
    ('hand_to_hand', {'label': 'Hand to hand'}),
    ('left_over_right', {'label': 'Left over right'}),
    ('right_over_left', {'label': 'Right over left'}),
]

edges = [
    (('closed', 'half_closed'), {'label': 'drop'}),
    (('closed', 'left_to_right'), {'label': 'drop'}),
    (('closed', 'hand_to_hand'), {'label': 'slide'}),

    (('half_closed', 'closed'), {'label': 'collect'}),
    (('half_closed', 'right_to_left'), {'label': 'slide'}),

    (('left_to_left', 'half_closed'), {'label': 'place-collect'}),
    (('left_to_left', 'right_to_left'), {'label': 'pass'}),
    (('left_to_left', 'left_over_right'), {'label': 'collect'}),
    (('left_to_left', 'right_over_left'), {'label': 'collect'}),

    (('left_to_right', 'closed'), {'label': 'collect'}),
    (('left_to_right', 'right_to_right'), {'label': 'pass'}),
    (('left_to_right', 'hand_to_hand'), {'label': 'collect'}),
    
    (('right_to_left', 'left_to_left'), {'label': 'pass'}),
    (('right_to_left', 'hand_to_hand'), {'label': 'collect'}),
    
    (('right_to_right', 'left_to_right'), {'label': 'pass'}),
    (('right_to_right', 'left_over_right'), {'label': 'collect'}),
    (('right_to_right', 'right_over_left'), {'label': 'collect'}),

    (('left_over_right', 'half_closed'), {'label': 'place-drop-collect'}),
    (('left_over_right', 'left_to_left'), {'label': 'drop'}),
    (('left_over_right', 'right_to_right'), {'label': 'drop'}),
    (('left_over_right', 'right_over_left'), {'label': 'cut'}),

    (('hand_to_hand', 'closed'), {'label': 'slide'}),
    (('hand_to_hand', 'left_to_right'), {'label': 'drop'}),
    (('hand_to_hand', 'right_to_left'), {'label': 'drop'}),

    (('right_over_left', 'left_to_left'), {'label': 'drop'}),
    (('right_over_left', 'right_to_right'), {'label': 'drop'}),
    (('right_over_left', 'left_over_right'), {'label': 'cut'}),
]


def add_nodes(graph, nodes):
    for node in nodes:
        if isinstance(node, tuple):
            graph.node(node[0], **node[1])
        else:
            graph.node(node)
    return graph


def add_edges(graph, edges):
    for edge in edges:
        if isinstance(edge[0], tuple):
            graph.edge(*edge[0], **edge[1])
        else:
            graph.edge(*edge)
    return graph

graph = graphviz.Digraph(format='png')
graph = add_nodes(graph, nodes)
graph = add_edges(graph, edges)
graph.render('frame_transitions.dot')
