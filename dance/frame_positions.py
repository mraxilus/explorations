#!/usr/bin/env python3

import itertools
import pprint
print_pretty = pprint.PrettyPrinter().pprint

lead = ['Empty', 'Left', 'Right']
follow = ['empty', 'left', 'right']
modifiers = ['base', 'lock', 'wrap']

positions = [list(zip(lead, p)) for p in itertools.permutations(follow)]

print(len(positions))
print_pretty(positions)
