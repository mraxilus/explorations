{.experimental: "code_reordering".}

import algorithm
import sequtils
import sets
import strutils
import sugar
import times
import options



type
  Stacks = seq[seq[char]]
  Move = tuple
    count: int
    source: int
    destination: int



benchmark "aoc2022_5a": # DHBJQJCCW
  let
    sections = read_file("input.txt").split("\n\n")
    moves = sections[1].parse_moves
  var
    stacks = sections[0].parse_stacks

  for move in moves: stacks.move_iterative(move)
  for stack in stacks: stdout.write(stack[^1])
  stdout.write('\n')

benchmark "aoc2022_5b": # WJVRLSJJT
  let
    sections = read_file("input.txt").split("\n\n")
    moves = sections[1].parse_moves
  var
    stacks = sections[0].parse_stacks

  for move in moves: stacks.move_simultaneous(move)
  for stack in stacks: stdout.write(stack[^1])
  stdout.write('\n')



template benchmark(name: string, code: untyped) =
  block:
    let time_start = epoch_time()
    code
    let
      time_elapsed = epoch_time() - time_start
      duration = time_elapsed.format_float(format = ff_decimal, precision = 5)
    echo("[", name, "] time.cpu = ", duration, "s")


func parse_moves(text: string): seq[Move] =
  # Parse moves from text to tuple representation.
  text
    .replace("move ", "")
    .replace("from ", "")
    .replace("to ", "")
    .split('\n')
    .mapIt(it.split(' ').map(parse_int))
    .mapIt((it[0], it[1], it[2]))


proc parse_stacks(text: string): Stacks =
  # Parse stacks from text representation to sequences of characters.
  let
    layers = text
      .split('\n')[0..^2]  # Ignore index row.
      .mapIt(zip((0..it.len - 2).to_seq, it[1..^1]))
      .map(item => item
        # Include only informative characters.
        .filterIt(it[0] mod 4 == 0)
        .unzip()[1]
      )
      .reversed
    count = len(layers[0])

  # Populate data structure with parsed information.
  for _ in 0..<count: result.add(@[])
  for layer in layers:
    for i, item in layer:
      if item != ' ': result[i].add(item)


proc move_iterative(stacks: var Stacks, move: Move) =
  # Move items from/to specified stacks iteratively.
  # I.e the original stack order will be reversed.
  for _ in 1..move.count:
    stacks[move.destination - 1].add(stacks[move.source - 1].pop)


proc move_simultaneous(stacks: var Stacks, move: Move) =
  # Move items from/to specified stacks simultaneously.
  # I.e. the original stack order will be maintained.
  stacks[move.destination - 1].add(stacks[move.source - 1][^move.count..^1])
  stacks[move.source - 1] = stacks[move.source - 1][0 ..< ^move.count]
