import strutils
import sequtils
import sugar
import std/algorithm
import times

template benchmark(name: string, code: untyped) =
  block:
    let time_start = epoch_time()
    code
    let time_elapsed = epoch_time() - time_start
    let duration = time_elapsed.format_float(format = ff_decimal, precision =5)
    echo("CPU Time [", name, "] ", duration, "s")
  
func get_max_sum_of_calories(inventory: string): int =
  # Determine max sum of subsequences in a newline delimited string.
  inventory
    .strip()
    .split("\n\n")
    .map((xs) => xs
      .split()
      .map(parse_int)
      .foldl(a + b)
    )
    .max()

proc get_max_sum_of_top_three_calories(inventory: string): int =
  # Determine sum of sum of top three subsequences in a newline delimited string.
  var calories =  inventory
    .strip()
    .split("\n\n")
    .map((xs) => xs
      .split()
      .map(parse_int)
      .foldl(a + b)
    )
  calories.sort()
  return calories[^3..^1].foldl(a + b)

benchmark "aoc2022_1a":
  read_file("./input.txt")
    .get_max_sum_of_calories()
    .echo()

benchmark "aoc2022_1b":
  read_file("./input.txt")
    .get_max_sum_of_top_three_calories()
    .echo()
