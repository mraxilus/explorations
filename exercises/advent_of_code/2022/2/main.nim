import sequtils
import strutils
import sugar
import tables
import times


  
template benchmark(name: string, code: untyped) =
  block:
    let time_start = epoch_time()
    code
    let time_elapsed = epoch_time() - time_start
    let duration = time_elapsed.format_float(format=ff_decimal, precision=5)
    echo("CPU Time [", name, "] ", duration, "s")



type 
  Choice = range[1..3]
  Key = tuple
    left: string
    right: string
  Outcome = tuple
    left: Choice
    right: Choice


  
const 
  keys_left = ["A", "B", "C"]
  keys_right = ["X", "Y", "Z"]
  to_key_left = zip(keys_right, keys_left).to_table()
  to_choice = zip(keys_left, (1..high(Choice)).to_seq()).to_table()



func score(outcome: Outcome): int =
  # Score outcome of rock, paper, scissors using sum of position and outcome factor.
  let (left, right) = outcome
  if left == right:  # Draw
    3 + right
  elif (left == right - 1) or (left == 3 and right == 1):  # Win
    6 + right
  else:  # Loss
    0 + right


func score_as_option(key: Key): int =
  # Score outcome of rock, paper, scissors using key as option to choose.
  # I.e. X=rock, Y=paper, Z=scissors.
  let 
    left: Choice = to_choice[key.left]
    right: Choice = to_choice[to_key_left[key.right]]
  (left, right).score()


func score_as_result(key: Key): int =
  # Score outcome of rock, paper, scissors using key as result to obtain.
  # I.e. X=lose, Y=draw, Z=win.
  let 
    left: Choice = to_choice[key.left]
    right: Choice = case key.right:
      of "Z":  # Win
        (left.mod(3)) + 1
      of "X":  # Lose
        ((left + 1).mod(3)) + 1
      else: # Draw
        left
  (left, right).score()



benchmark "aoc2022_2a":
  read_file("input.txt")
    .strip()
    .split("\n")
    .map((xs) => xs.split())
    .map((xs) => score_as_option((xs[0], xs[1])))
    .foldl(a + b)
    .echo()

benchmark "aoc2022_2b":
  read_file("input.txt")
    .strip()
    .split("\n")
    .map((xs) => xs.split())
    .map((xs) => score_as_result((xs[0], xs[1])))
    .foldl(a + b)
    .echo()
