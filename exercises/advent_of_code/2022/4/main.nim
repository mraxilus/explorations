import sequtils
import sets
import strutils
import sugar
import times
import options



template benchmark(name: string, code: untyped) =
  block:
    let time_start = epoch_time()
    code
    let
      time_elapsed = epoch_time() - time_start
      duration = time_elapsed.format_float(format = ff_decimal, precision = 5)
    echo("[", name, "] time.cpu = ", duration, "s")



func parse_slices(input: string): seq[Option[Slice[int]]] =
  # Parse two ranges in string representation into valid standard datatype.
  # I.e. the representation takes the form "a-b,c-d", where a-d are numbers.
  let slices = input
    .split(',')
    .map(r => r.split('-').map(i => (
      try: some(parse_int(i))
      except: none(int)
    )))
  slices.map(slice => (
    if slice.any(bound => bound.is_none): none(Slice[int])
    else: some(get(slice[0])..get(slice[1]))
  ))


func is_parsed(slices: seq[Option[Slice[int]]]): bool =
  # Determine if slices are present and valid.
  slices.all(s => s.is_some) and len(slices) == 2 


func is_subset_either(x, y: Slice[int]): bool =
  # Determine either of two numerical ranges are a subset of the other.
  (x.a >= y.a and x.b <= y.b) or (x.a <= y.a and x.b >= y.b)


func is_disjoint(x, y: Slice[int]): bool =
  # Determine if two numerical ranges are non-overlapping.
  x.b < y.a or y.b < x.a



benchmark "aoc2022_4a":  # 305
  read_file("input.txt")
    .split('\n')
    .map(parse_slices)
    .filter(is_parsed)
    .map(xs => is_subset_either(get(xs[0]), get(xs[1])))
    .count(true)
    .echo


benchmark "aoc2022_4b":  # 811
  read_file("input.txt")
    .split('\n')
    .map(parse_slices)
    .filter(is_parsed)
    .map(xs => not is_disjoint(get(xs[0]), get(xs[1])))
    .count(true)
    .echo
