import sequtils
import sets
import strutils
import sugar
import times
import options



template benchmark(name: string, code: untyped) =
  block:
    let time_start = epoch_time()
    code
    let
      time_elapsed = epoch_time() - time_start
      duration = time_elapsed.format_float(format = ff_decimal, precision = 5)
    echo("[", name, "] time.cpu = ", duration, "s")



const
  len_alphabet = len({'a'..'z'})
  offset_lowercase = int('a')
  offset_uppercase = int('A')



func find_common_chars(a: string, b: string): string =
  # Find common characters across two strings.
  cast[string](intersection(a.to_hash_set, b.to_hash_set).to_seq)


func score_first_char(s: string): Option[int] =
  # Score first character (if valid) by its place in an ordered alphabet.
  # I.e. The ordered set {a..z,A..Z} translates to {1..52}.
  if len(s) == 0: return
  let value = int(s[0])
  if value >= offset_uppercase and value <= offset_uppercase + len_alphabet: # Uppercase
    some(value - (offset_uppercase - 1) + len_alphabet)
  elif value >= offset_lowercase and value <= offset_lowercase + len_alphabet: # Lowercase
    some(value - (offset_lowercase - 1))
  else: none(int)



benchmark "aoc2022_3a": # 8233
  read_file("input.txt")
    .strip
    .split("\n")
    .map(s => (
      let offset = len(s) div 2
      find_common_chars(s[0..<offset], s[offset..^1])
        .score_first_char
    ))
    .foldl(some(get(a, 0) + get(b, 0)))
    .echo


benchmark "aoc2022_3b": # 2821
  let inputs =
    read_file("input.txt")
    .strip
    .split("\n")
  inputs
    .distribute(len(inputs) div 3)
    .map(xs => xs
      .foldl(find_common_chars(a, b))
      .score_first_char
    )
    .foldl(some(a.get(0) + b.get(0)))
    .echo
