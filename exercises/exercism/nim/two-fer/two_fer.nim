proc twoFer*(name = "you"): string =
  ## Produce customized message of the form, `One for [name], one for me.`.
  ##
  ## Where if `name` is not given, it defaults to `you`.
  "One for " & name & ", one for me."
