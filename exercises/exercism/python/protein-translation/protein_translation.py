from itertools import takewhile
from typing import List, NewType

RNA = NewType("RNA", str)
AminoAcid = NewType("AminoAcid", str)
Codon = NewType("Codon", str)


def proteins(strand: RNA) -> List[AminoAcid]:
    """Translate RNA strand into resulting protien."""
    codons = takewhile(lambda c: len(c) == 3, translate_rna(strand))
    proteins = [translate_codon(c) for c in codons]
    return list(takewhile(lambda p: p != "STOP", proteins))


def translate_rna(rna: RNA) -> List[Codon]:
    """Translate RNA sequence into its constituent codons."""
    return [rna[i : i + 3] for i in range(0, len(rna), 3)]


def translate_codon(codon: Codon) -> AminoAcid:
    """Translate codon into its amino acid name."""
    return {
        "AUG": "Methionine",
        "UUU": "Phenylalanine",
        "UUC": "Phenylalanine",
        "UUA": "Leucine",
        "UUG": "Leucine",
        "UCU": "Serine",
        "UCC": "Serine",
        "UCA": "Serine",
        "UCG": "Serine",
        "UAU": "Tyrosine",
        "UAC": "Tyrosine",
        "UGU": "Cysteine",
        "UGC": "Cysteine",
        "UGG": "Tryptophan",
        "UAA": "STOP",
        "UAG": "STOP",
        "UGA": "STOP",
    }[codon]
