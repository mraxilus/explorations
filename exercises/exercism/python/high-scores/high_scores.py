def latest(scores: list) -> int:
    """Retrieve the latest score."""
    return scores[-1]


def personal_best(scores: list) -> int:
    """Retrieve the highest score."""
    return max(scores)


def personal_top_three(scores: list) -> list:
    """Retrieve the three highest scores."""
    return sorted(scores, reverse=True)[:3]
