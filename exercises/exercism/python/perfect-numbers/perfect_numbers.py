def classify(number: int) -> bool:
    """Determine if a number is perfect."""
    aliquot_sum = sum(factor(number)[:-1])

    if number == aliquot_sum:
        return "perfect"

    if number < aliquot_sum:
        return "abundant"

    if number > aliquot_sum:
        return "deficient"


def factor(number: int) -> list:
    """Determine the factors of a number."""
    if number < 1:
        raise ValueError(f"{number} is not a whole number.")

    # Reduce calculation by clipping to half of number.
    candidates = range(1, number // 2 + 1)
    return list(filter(lambda c: number % c == 0, candidates)) + [number]
