def is_pangram(sentence: str = None) -> str:
    """Determine if sentence is a pangram."""
    return set(sentence) == set("abcdefghijklmnopqrstuvwxyz")
