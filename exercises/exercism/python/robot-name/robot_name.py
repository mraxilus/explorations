import random
import string


class Robot:
    names = set()

    def __init__(self):
        """Create new robot and assign it unique name."""
        self.name = None
        self.set_name()

    def set_name(self):
        """Assign robot unique name if it doesn't already have one."""
        while self.name == None:
            # Generate new name candidate.
            letters = "".join(random.choices(string.ascii_uppercase, k=2))
            number = random.randint(0, 999)
            name = f"{letters}{number:03d}"

            # Assign name to robot if not in use.
            if name not in Robot.names:
                Robot.names.add(name)
                self.name = name

    def reset(self):
        """Reassign new name to robot."""
        self.name = None
        self.set_name()
