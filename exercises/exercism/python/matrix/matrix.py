class Matrix:
    """Data structure for parsing and accessing matrix values."""

    def __init__(self, matrix_string: str) -> None:
        """Construct matrix from string representation."""
        lines = matrix_string.splitlines()
        rows = [l.split() for l in lines]
        self.matrix = [[int(v) for v in r] for r in rows]

    def row(self, index: int) -> list:
        "Retrieve specific row from matrix." ""
        return self.matrix[index - 1]

    def column(self, index: int) -> list:
        "Retrieve specific column from matrix." ""
        return [row[index - 1] for row in self.matrix]
