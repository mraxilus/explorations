def is_armstrong_number(number: int) -> bool:
    """Verify if a number is an armstrong number."""
    numbers = [int(n) for n in str(number)]
    numbers_raised = [n ** len(numbers) for n in numbers]
    return number == sum(numbers_raised)
