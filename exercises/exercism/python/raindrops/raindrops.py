def convert(number: int) -> str:
    """Create onomatopoeia out of a number."""

    def make_sound(number: int, factor: int, sound: str) -> str:
        """Make sound if factor is a factor of number."""
        return sound if number % factor == 0 else ""

    # Setup mappings between the factors and sounds.
    mappings = [(3, "Pling"), (5, "Plang"), (7, "Plong")]

    # Generate the appropriate sound based on the number.
    result = ""
    for (factor, sound) in mappings:
        result += make_sound(number, factor, sound)

    return result or str(number)
