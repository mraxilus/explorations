module Bob exposing (hey)


hey : String -> String
hey remark =
    let
        remark_ =
            String.trim remark

        allCapital =
            remark_ == String.toUpper remark_ && remark_ /= String.toLower remark_

        isQuestion =
            String.endsWith "?" remark_
    in
    if not allCapital && isQuestion then
        "Sure."

    else if allCapital && not isQuestion then
        "Whoa, chill out!"

    else if allCapital && isQuestion then
        "Calm down, I know what I'm doing!"

    else if String.isEmpty remark_ then
        "Fine. Be that way!"

    else
        "Whatever."
