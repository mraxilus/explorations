let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    exercism
    elmPackages.elm
    elmPackages.elm-test
    elmPackages.elm-format
    nim
  ];
}
