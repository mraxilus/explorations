import numpy

def find_closest_to_zero(temperatures: list) -> int:
    '''Find the closest reading to zero.'''
    minimum_distance = numpy.inf
    for temperature in temperatures:
        
        # Replace current closest reading with closer reading.
        if abs(temperature) < abs(minimum_distance):
            minimum_distance = temperature
            
        # Replace negative readings with positive.
        elif (abs(temperature) == abs(minimum_distance) and
              minimum_distance < 0 and 
              temperature > 0):
            minimum_distance = temperature
    
    result = minimum_distance if minimum_distance != numpy.inf else 0
    return result
    
    
# Convert inputs into usable data structure.
input()  # Ignore size of readings.
temperatures = list(map(int, input().split()))
print(find_closest_to_zero(temperatures))
