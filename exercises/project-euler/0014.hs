import Data.List

sequence' :: Int -> Int
sequence' n
    | n <= 1    = 1
    | even n    = 1 +  sequence' (n `div` 2)
    | otherwise = 1 + sequence' ((3 * n) + 1)



collatz = map sequence' [1..999999]

collatz' =  zip [1..999999] collatz

problem_14 = scanl (\(a,b) (c,d) -> if b > d then (a,b) else (c,d)) (0, 0) collatz'

