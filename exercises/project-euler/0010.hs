-- problem 10
-- ==========
-- the sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
--
-- find the sum of all the primes below two million.

import qualified Data.Array as A

-- TODO(mraxilus): implement efficient local sieve
primes :: [Int]
primes = 2: 3: sieve (tail primes) 3 []
   where
    sieve (p:ps) x fs = [i*2 + x | (i,e) <- A.assocs a, e]
                        ++ sieve ps (p*p) fs'
     where
      q     = (p*p-x)`div`2
      fs'   = (p,0) : [(s, rem (y-q) s) | (s,y) <- fs]
      a     = A.accumArray (\ b c -> False) True (1,q-1)
                           [(i,()) | (s,y) <- fs, i <- [y+s,y+s+s..q]]

solveProblem :: Int
solveProblem = sum $ takeWhile (< 2000000) primes
