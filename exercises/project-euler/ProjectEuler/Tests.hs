module ProjectEuler.Tests
( isPrime
) where

import qualified ProjectEuler.Sets as S

isPrime :: (Integral a) => a -> Bool
isPrime n = elem e (takeWhile (<= e) S.primes)
  where e = toInteger n
