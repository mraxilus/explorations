module ProjectEuler.Functions
( choose
, divisorsOf
, factorial
, groupsOf
, maxPathSum
, splitOn
) where

import qualified Data.Text as T
import qualified Data.Set as S
import qualified Math.NumberTheory.Primes.Factorisation as F

choose :: (Integral a) => a -> a -> a
choose n k = (factorial n) `div` ((factorial k) * (factorial (n - k)))

-- TODO(mraxilus): implement local version of divisors
divisorsOf :: (Integral a) => a -> [Integer]
divisorsOf n = S.elems (F.divisors (toInteger n))

factorial :: (Integral a) => a -> a
factorial n = product [1..n]

groupsOf :: (Integral a) => Int -> [a] -> [[a]]
groupsOf n xs
  | length xs < n = []
  | otherwise  = take n xs : groupsOf n (tail xs)

maxPathSum :: (Integral a) => [[a]] -> a
maxPathSum ((x:[]):[])   = x
maxPathSum (x:y:zs)      = maxPathSum (sums:zs)
  where sums          = zipWith (+) (selections x) y
        selections xs = map (maximum) (groupsOf 2 xs)

splitOn :: String -> String -> [String]
splitOn x y = map (T.unpack) splitText
  where splitText = T.splitOn (T.pack x) (T.pack y) 
