module ProjectEuler.Sets
( pandigtals
, primes
) where

import qualified Data.List as L
import qualified Math.NumberTheory.Primes.Sieve as S

pandigtals :: (Integral a) => a -> [[a]]
pandigtals base = L.permutations [0..(base - 1)]

-- TODO(mraxilus): implement local prime sieve.
primes :: [Integer]
primes = S.primes
