module ProjectEuler.Conversions
( digitsOf
, digitsToInt
) where

import qualified Data.Char as C

digitsOf :: (Integral a) => a -> [a]
digitsOf 0 = []
digitsOf x = digitsOf (x `div` 10) ++ [x `mod` 10]

digitsToInt :: (Integral a, Read a) => [Int] -> a
digitsToInt xs = read (map C.intToDigit xs)
