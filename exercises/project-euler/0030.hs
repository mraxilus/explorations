import Data.Char

isPowerSum p n = n == sum (map (^p) digits)
    where digits = map digitToInt (show n)

powerSumsOf p = scanl (+) 0 powerSums
    where powerSums = filter (isPowerSum p) [2..]
