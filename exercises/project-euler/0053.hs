#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 53
-- ==========
-- there are exactly ten ways of selecting three from five,
--   12345:
--
-- 123,
--   124,
--   125,
--   134,
--   135,
--   145,
--   234,
--   235,
--   245,
--   and 345
--
-- in combinatorics,
--   we use the notation,
--   5c3 = 10.
--
-- in general,
--   ncr =  n! / r!(n−r)!,
--   where r ≤ n, n! = n×(n−1)×...×3×2×1,
--   and 0! = 1.
--
-- it is not until n = 23,
--   that a value exceeds one-million: 23c10 = 1144066.
--
-- how many,
--   not necessarily distinct,
--   values of  ncr,
--   for 1 ≤ n ≤ 100,
--   are greater than one-million?

import qualified ProjectEuler.Functions as F

main :: IO ()
main = do
  let solution = length [ncr | n <- [1..100],
                               r <- [1..n],
                               let ncr = F.choose n r,
                               ncr > 1000000]
  print solution
