-- problem 9
-- =========
-- a pythagorean triplet is a set of three natural numbers, a < b < c, for which,
--   a^2 + b^2 = c^2
--
-- for example,
--   32 + 42 = 9 + 16 = 25 = 52.
--
-- there exists exactly one pythagorean triplet for which a + b + c = 1000.
-- find the product abc.

triplets :: Int -> [[Int]]
triplets l = [[a, b, c] | m <- [2..limit],
                          n <- [1..(m - 1)],
                          let a = m^2 - n^2,
                          let b = 2 * m * n,
                          let c = m^2 + n^2,
                          a + b + c == l ]
    where limit = floor .sqrt . fromIntegral $ l

solveProblem :: Int
solveProblem = product . head . triplets $ 1000
