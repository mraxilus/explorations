import Data.List

sequenceTerms x y = length $ sort $ nub [n | a <- [x..y],
                                             b <- [x..y],
                                             let n = a^b ]
