#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 23
-- ==========
-- a perfect number is a number for which the sum of its proper divisors is exactly equal to the number.
-- for example,
--   the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28,
--   which means that 28 is a perfect number.
--
-- a number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.
--
-- as 12 is the smallest abundant number,
--   1 + 2 + 3 + 4 + 6 = 16,
--   the smallest number that can be written as the sum of two abundant numbers is 24.
-- by mathematical analysis,
--   it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers.
-- however,
--   this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.
--
-- find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.

module Main where

import qualified Data.List.Ordered as O
import qualified Data.Set as S
import qualified ProjectEuler.Functions as F

main :: IO ()
main = do
  let limit = 28123
  let sumsUnderLimit = abundantSumsUnder limit
  let nonAbundantSums = filter (\x -> not (elem x sumsUnderLimit)) [1..]
  let solution = sum $ takeWhile (< limit) nonAbundantSums
  print solution

abundantSumsUnder :: Integer -> [Integer]
abundantSumsUnder limit = S.toList (S.fromList duplicatedSums)
  where duplicatedSums = concat (takeWhile underLimit availableSums)
        underLimit x = not (null x) && head x < limit
        availableSums = map (takeWhile (< limit)) abundantSums

abundantSums :: [[Integer]]
abundantSums = [[n | i <- abundants, let n = i + j]
                   | j <- abundants]

abundants :: [Integer]
abundants = filter isAbundant [1..]

isAbundant :: Integer -> Bool
isAbundant n = sum (F.divisorsOf n) > n + n
