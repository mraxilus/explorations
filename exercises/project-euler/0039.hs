#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 31
-- ==========
-- if p is the perimeter of a right angle triangle with integral length sides,
--   {a,b,c},
--   there are exactly three solutions for p = 120.
--
-- {20,48,52},
--   {24,45,51},
--   {30,40,50}
--
-- for which value of p ≤ 1000,
--   is the number of solutions maximised?
--
-- insights
-- --------
-- - longest side must be between a third and a half of the perimeter.
-- - middle side must be between the difference of the longest side,
--     and the perimeter.
-- - shortest side can be solved for given the lengths of other two sides.

module Main where

main :: IO ()
main = do
  let solutionCounts = map (\p -> (length (rightTrianglesFor p), p)) [1..1000]
  let solution = snd (maximum solutionCounts)
  print solution


rightTrianglesFor :: (Integral a) => a -> [[a]]
rightTrianglesFor p = [[a, b, c] | c <- [p `div` 3..(p `div` 2) - 1],
                                   b <- [(p - c) `div` 2..c],
                                   let a = p - (b + c),
                                   a^2 + b^2 == c^2]
