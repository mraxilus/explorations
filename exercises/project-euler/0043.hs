#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 43
-- ==========
-- the number,
--   1406357289,
--   is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order,
--   but it also has a rather interesting sub-string divisibility property.
--
-- let d1 be the 1st digit,
--   d2 be the 2nd digit,
--   and so on.
-- in this way, we note the following:
--
-- - d₂d₃d₄ = 406 is divisible by 2
-- - d₃d₄d₅ = 063 is divisible by 3
-- - d₄d₅d₆ = 635 is divisible by 5
-- - d₅d₆d₇ = 357 is divisible by 7
-- - d₆d₇d₈ = 572 is divisible by 11
-- - d₇d₈d₉ = 728 is divisible by 13
-- - d₈d₉d₁₀ = 289 is divisible by 17
--
-- find the sum of all 0 to 9 pandigital numbers with this property.

module Main where

import qualified ProjectEuler.Functions as F
import qualified ProjectEuler.Sets as S

main :: IO ()
main = do
  let pandigtalsAsLists = filter hasProperty (S.pandigtals 10)
  let pandigtalsAsInts = map F.digitsToInt pandigtalsAsLists
  let solution = sum pandigtalsAsInts
  print solution

hasProperty :: [Int] -> Bool
hasProperty xs = all (\(a, b) -> a `mod` b == 0) groups
  where groups = zip substringsAsInts S.primes
        substringsAsInts = map F.digitsToInt substringsAsLists
        substringsAsLists = F.groupsOf 3 (tail xs)

