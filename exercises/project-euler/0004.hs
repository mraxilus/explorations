-- problem 4
-- =========
-- a palindromic number reads the same both ways. 
-- the largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
--
-- find the largest palindrome made from the product of two 3-digit numbers.

import qualified Data.List as L
import qualified Data.Ord as O

solveProblem :: Int
solveProblem = fst $ L.maximumBy (O.comparing fst) products 
  where products = [ (n, (x, y)) | x <- [100..999],
                                   y <- [x..999],
                                   let n = x*y,
                                   let s = show n,
                                   s == reverse s ]
