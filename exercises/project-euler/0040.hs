#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 40
-- ==========
-- an irrational decimal fraction is created by concatenating the positive integers:
--
-- 0.123456789101112131415161718192021...
--
-- it can be seen that the 12th digit of the fractional part is 1.
--
-- if dn represents the nth digit of the fractional part,
--   find the value of the following expression.
--
-- d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000

import qualified ProjectEuler.Conversions as C

main :: IO ()
main = do
  let places = map (10^) [0..6]
      digits = map (champernowneDigits!!) places
      solution = product digits
  print solution

champernowneDigits :: (Integral a) => [a]
champernowneDigits = 0 : concat (map C.digitsOf [1..])
