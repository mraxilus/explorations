
import Data.Char

googols = [ a ^ b | a <- [1..99], b <- [1..99] ]

googolSums = map (\x -> sum (map digitToInt (show x))) googols

maxGoogolSum = maximum googolSums
