-- let d(k) be the sum of all divisors of k.
-- we define the function s(n) = ∑1≤i≤n ∑1≤j≤n d(i·j).
-- for example, s(3) = d(1) + d(2) + d(3) + d(2) + d(4) + d(6) + d(3) + d(6) + d(9) = 59.
--
-- you are given that s(103) = 563576517282 and s(105) mod 109 = 215766508.
-- find s(1011) mod 109.

import qualified Data.List as L

-- optimization from: http://stackoverflow.com/a/11984461/724124
divisors n = (1:) $ L.nub $ concat $ [ [x, div n x] | x <- [2..limit], rem n x == 0 ] 
     where limit = (floor.sqrt.fromIntegral) n

matrixSize = 10^2

-- TODO(mraxilus): create memorization structure or hash table with key expiry.
solveProblem =  sum $ map (\k -> if k > 1 then sum (divisors k ++ [k]) else 1) matrix
  where matrix = [i * j | i <- [1..matrixSize], j <- [1..matrixSize]]
