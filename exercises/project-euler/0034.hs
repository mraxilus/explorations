#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 34
-- ==========
-- 145 is a curious number,
--   as 1! + 4! + 5! = 1 + 24 + 120 = 145.
--
-- find the sum of all numbers which are equal to the sum of the factorial of their digits.
--
-- note: as 1! = 1 and 2! = 2 are not sums they are not included.
--
-- insights
-- --------
-- - for any factorion n of d digits, 10^(d-1) ≤ n ≤ 9!d,
--     thus the first upper limit is 9999999 as d ≥ 8 fails this assertion.
-- - the highest factorial sum given by a seven digit sum is 9!*7,
--     thus a new upper limit of 2540160 is achieved.

module Main where

import qualified ProjectEuler.Conversions as C
import qualified ProjectEuler.Functions as F

main :: IO ()
main = do
  let solution = sum [n | n <- [3..2540160], isFactorion n]
  print solution

isFactorion :: (Integral a) => a -> Bool
isFactorion n = n == sumOfFactorialsOf n

sumOfFactorialsOf :: (Integral a) => a -> a
sumOfFactorialsOf n = sum (map F.factorial digits)
  where digits = C.digitsOf n

