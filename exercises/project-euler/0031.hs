#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 31
-- ==========
-- in england the currency is made up of pound,
--   £,
--   and pence,
--   p,
--   and there are eight coins in general circulation:
--
-- 1p,
--   2p,
--   5p,
--   10p,
--   20p,
--   50p,
--   £1 (100p) and £2 (200p).
--
-- it is possible to make £2 in the following way:
--
-- 1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
--
-- how many different ways can £2 be made using any number of coins?
--
-- insights
-- --------
-- - dynamic programming.
-- - set of all possible combinations can be divided into:
--   a. set of combinations with one or more of a coin.
--   b. set of combinations with none of a coin.

module Main where

import qualified Data.List as L

main :: IO ()
main = do
  let solution = chooseCoins [200, 100, 50, 20, 10, 5, 2, 1] 200
  print solution

chooseCoins :: (Integral a) => [a] -> a -> a
chooseCoins cs n
  | length cs == 0 = 0
  | n < 0  = 0
  | n == 0 = 1
  | otherwise = choicesWithCoin + choicesWithoutCoin
  where choicesWithCoin = chooseCoins coinsSorted (n - (head coinsSorted))
        choicesWithoutCoin = chooseCoins (tail coinsSorted) n
        coinsSorted = reverse (L.sort cs)
