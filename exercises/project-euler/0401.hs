#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 401
-- ===========
-- the divisors of 6 are 1,
--  2,
--  3 and 6.
-- the sum of the squares of these numbers is 1+4+9+36=50.
--
-- let sigma2(n) represent the sum of the squares of the divisors of n.
-- thus sigma2(6)=50.
--
-- let SIGMA2 represent the summatory function of sigma2,
--   that is SIGMA2(n)=∑sigma2(i) for i=1 to n.
-- the first 6 values of SIGMA2 are: 1,6,16,37,63 and 113.
-- find SIGMA2(1015) modulo 109.

module Main where

import qualified ProjectEuler.Functions as F

main :: IO ()
main = do
  let solution = (sigma2' (10^15)) `mod` (10^9)
  print solution

sigma2 :: (Integral a) => a -> Integer
sigma2 n = sum squares
  where squares = map (^2) divisors
        divisors = F.divisorsOf n

sigma2' :: (Integral a) => a -> Integer
sigma2' n = sum (map sigma2 [1..n])
