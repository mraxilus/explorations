import Data.List

properDivisors :: (Integral a) => a -> [a]
properDivisors n = [x | x <- [1..n `div` 2],
                        n `mod` x == 0 ]

amicablePair :: (Integral a) => a -> Maybe a
amicablePair a
    | a == b = Nothing
    | a == dOf b = Just b
    | otherwise = Nothing
        where dOf x = sum (properDivisors x)
              b = dOf a

getAmicablePair :: (Integral a) => a -> [a]
getAmicablePair a = case amicablePair a of
            Just b -> [a,b]
            Nothing -> []

amicables = nub $ foldr (++) [] ams
    where ams = map getAmicablePair [1..]
