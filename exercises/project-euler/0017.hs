module PE17 where

import Data.List

ones = [ "","one","two","three","four","five","six","seven","eight"
       , "nine" ]

teens = [ "ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen"
        , "seventeen","eighteen","nineteen" ]

tens = [ "","","twenty","thirty","forty","fifty","sixty","seventy","eighty"
       , "ninety" ]

aboveTens = ["","","","hundred","thousand","million","billion"]

digitToWord :: [Int] -> [String]
digitToWord xs = map digitToWord' nums
    where nums = [n | n <- map seperate strs]
          strs = [s | s <- map show xs]

seperate [] = []
seperate (x:xs) = (read [((show x)!!1)] :: Int) : seperate xs


digitToWord' :: [Int] -> String
digitToWord' xs
    | xs == [] = []
    | length xs == 1 = ones!!(head xs)
    | length xs == 2 = if head xs == 1
                       then teens!!(head $ tail xs)
                       else tens!!(head xs) ++ digitToWord' (tail xs)
    | length xs == 3 = if head xs == 0
                       then checkForAnd xs ++ digitToWord' (tail xs)
                       else aboveTen xs ++ checkForAnd xs
                                        ++ digitToWord' (tail xs)
    | otherwise = if head xs == 0
                  then digitToWord' (tail xs)
                  else aboveTen xs ++ digitToWord' (tail xs)
    where aboveTen xs = ones!!(head xs) ++ aboveTens!!(length xs)
          checkForAnd xs = if sum (tail xs) /= 0
                           then "and"
                           else ""

problem_17 n = sum [x | y <- digitToWord[1..n], let x = length y]
