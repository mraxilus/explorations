-- problem 19
-- ==========
-- you are given the following information, but you may prefer to do some research for yourself.
--
-- - 1 jan 1900 was a monday.
-- - thirty days has september,
-- - april, june and november.
-- - all the rest have thirty-one,
-- - saving february alone,
-- - which has twenty-eight, rain or shine.
-- - and on leap years, twenty-nine.
-- - a leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
--
-- how many sundays fell on the first of the month during the twentieth century (1 jan 1901 to 31 dec 2000)?

import qualified Data.Time as T
import qualified Data.Time.Calendar.WeekDate as W
import qualified Data.Tuple.Select as S

isSunday :: T.Day -> Bool
isSunday d = S.sel3 (W.toWeekDate d) == 7 

isFirstOfMonth :: T.Day -> Bool
isFirstOfMonth d = S.sel3 (T.toGregorian d) == 1 

countSundays :: T.Day -> T.Day -> Int
countSundays begin end = length [d | d <- [begin..end],
                                     isSunday d,
                                     isFirstOfMonth d]

solveProblem = countSundays (T.fromGregorian 1901 1 1) 
                            (T.fromGregorian 2000 12 1)
