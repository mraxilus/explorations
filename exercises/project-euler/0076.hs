#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 76
-- ==========
-- it is possible to write five as a sum in exactly six different ways:
--
-- 4 + 1
-- 3 + 2
-- 3 + 1 + 1
-- 2 + 2 + 1
-- 2 + 1 + 1 + 1
-- 1 + 1 + 1 + 1 + 1
--
-- how many different ways can one hundred be written as a sum of at least two positive integers?

module Main where

import qualified Data.List as L

main :: IO ()
main = do
  let ones = take 100 [1,1..]
      solution = partitions 100 ones ones
  print solution

partitions n xs ys
  | n == 1 = 0
  | length xs < n || z > n = partitions (n - 1) xs ys
  | otherwise = 1 + partitions n (z:zs) ys + partitions (n - 1) xs ys
  where z = sum $ take n (L.sort xs)
        zs = drop n (L.sort xs)
