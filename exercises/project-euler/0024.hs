import Data.List

lexPerms :: (Eq a) => [a] -> [[a]]
lexPerms [] = [[]]
lexPerms xs = [x:ys | x <- xs, ys <- lexPerms (delete x xs)]

