-- problem 28
-- ==========
-- starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:
--
-- 21 22 23 24 25
-- 20  7  8  9 10
-- 19  6  1  2 11
-- 18  5  4  3 12
-- 17 16 15 14 13
--
-- it can be verified that the sum of the numbers on the diagonals is 101.
--
-- what is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
--
-- insights
-- ========
-- every four elements the skips until the next valid element increases by 2.
-- each row adds 4 more elements and are of odd length.
-- total elements to obtain = 1 + (4 * (side / 2))

solveProblem :: Int
solveProblem = sum $ take (spiralDiagonalsIn 1001) spiralDiagonals

spiralDiagonalsIn :: Int -> Int
spiralDiagonalsIn s = 1 + (4 * (s `div` 2))

spiralDiagonals :: [Int]
spiralDiagonals = spiralDiagonals' 0 0

spiralDiagonals' :: Int -> Int -> [Int]
spiralDiagonals' 0 _    = 1 : spiralDiagonals' 1 2
spiralDiagonals' i skip = spiral ++ spiralDiagonals' (last spiral) (skip + 2)
  where spiral = take 4 [x | x <- [i + skip, i + (skip * 2)..]]
