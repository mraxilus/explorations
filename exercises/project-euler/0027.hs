#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 27
-- ==========
-- euler discovered the remarkable quadratic formula:
--
-- n² + n + 41
--
-- it turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39.
-- however,
--   when n = 40,
--   402 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41,
--   and certainly when n = 41,
--   41² + 41 + 41 is clearly divisible by 41.
--
-- the incredible formula n² − 79n + 1601 was discovered,
--   which produces 80 primes for the consecutive values n = 0 to 79.
-- the product of the coefficients,
--   −79 and 1601,
--   is −126479.
--
-- considering quadratics of the form:
--
-- n² + an + b, where |a| < 1000 and |b| < 1000
--
-- where |n| is the modulus/absolute value of n
-- e.g. |11| = 11 and |−4| = 4
--
-- find the product of the coefficients,
--   a and b,
--   for the quadratic expression that produces the maximum number of primes for consecutive values of n,
--   starting with n = 0.

module Main where

import qualified ProjectEuler.Sets as S
import qualified ProjectEuler.Tests as T

main :: IO ()
main = do
  let possibleCoefficients = [(a, b) | a <- [-999..999], b <- [-999..999]]
  let primeCounts = map (\c -> (primeCountFor c, c)) possibleCoefficients
  let coefficients = snd (maximum primeCounts)
  let solution = (fst coefficients) * (snd coefficients)
  print solution

primeCountFor :: (Integral a) => (a, a) -> Int
primeCountFor c = length $ takeWhile (T.isPrime) (quadratics c)

quadratics :: (Integral a) => (a, a) -> [a]
quadratics c = map (formula c) [0..]
  where formula (a, b) n = (n * n) + (a * n) + b

