module PE22 where

import Data.Char
import Data.List
import System.Environment
import System.IO
import Text.Regex.Posix

totalScores :: [String] -> Integer
totalScores xs = sum (scores sorted 0)
    where sorted = sort xs

scores :: [String] -> Integer -> [Integer]
scores [] _ = []
scores xs index =
    (index+1) * sum (map (\x -> toInteger((ord x)-64)) (head xs))
        : scores (tail xs) (index+1)

main :: IO ()
main = do
    contents <- readFile "names.txt"
    let sections = contents =~ "[A-Z]*" :: [[String]]
        fSections = filter (\[x] -> length x > 0) sections
        names = foldl (++) [] fSections
    putStrLn $ show names
    putStrLn $ show (totalScores names)
