#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 20
-- ==========
-- n! means n × (n − 1) × ... × 3 × 2 × 1
--
-- for example,
--   10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
--   and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
--
-- find the sum of the digits in the number 100!

module Main where

import qualified ProjectEuler.Conversions as C
import qualified ProjectEuler.Functions as F

main :: IO ()
main = do
  let factorialDigits = C.digitsOf (F.factorial 100)
  let solution = sum factorialDigits
  print solution

