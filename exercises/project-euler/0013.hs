module Problem13 where

import System.Environment
import System.IO
import Data.Char

main = do
    contents <- readFile "13.txt"
    let digits = lines contents
        ints = map (\x -> read x :: Integer) digits
        sumOfInts = sum ints
        firstDigits = take 10 $ show sumOfInts
    putStrLn $ firstDigits

