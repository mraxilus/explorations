#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 357
-- ===========
-- consider the divisors of 30:
--   1,
--   2,
--   3,
--   5,
--   6,
--   10,
--   15,
--   30.
-- it can be seen that for every divisor d of 30,
--   d+30/d is prime.
--
-- find the sum of all positive integers n not exceeding 100 000 000,
--   such that for every divisor d of n,
--   d+n/d is prime.

module Main where

import ProjectEuler.Functions as F
import ProjectEuler.Tests as T

main :: IO ()
main = do
  let solution = take 3 [n | n <- [1..10^8], 
                      let divisors = F.divisorsOf n,
                      all (\d -> T.isPrime ((d + n) `div` d)) divisors]
  print solution
