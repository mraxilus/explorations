import Data.Char

sumOfDigits xs = sum . map digitToInt $ show xs

problem_16 xs = sumOfDigits xs
