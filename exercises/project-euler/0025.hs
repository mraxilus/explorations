import Data.List

fibs = 1 : 1 : zipWith (+) fibs (tail fibs)

problem_25 ds = element + 1
    where element = length $ takeWhile (\x -> length (show x) /= ds) fibs
