#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 52
-- ==========
-- it can be seen that the number,
--   125874,
--   and its double,
--   251748,
--   contain exactly the same digits,
--   but in a different order.
--
-- find the smallest positive integer,
--   x,
--   such that 2x,
--   3x,
--   4x,
--   5x,
--   and 6x,
--   contain the same digits.

import qualified Data.List as L
import qualified ProjectEuler.Conversions as C

main :: IO ()
main = do
  let solution = head permutedMultiples
  print solution

permutedMultiples :: (Integral a) => [a]
permutedMultiples = [n | n <- [1..], digitsEquate (map (*n) [1..6])]

digitsEquate :: (Integral a) => [a] -> Bool
digitsEquate xs = all (== (head digitsSorted)) (tail digitsSorted)
  where digitsSorted = map (L.sort . C.digitsOf) xs
