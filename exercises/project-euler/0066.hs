import Data.List (sort)

minSoln :: (Integral a) => a -> (a,a)
minSoln d = head [(x,d) | x <- [1..],
                          y <- [1..x^2 `div` d],
                          x^2 - d*y^2 == 1 ]

isSquare :: (Integral a) => a -> Bool
isSquare n = n == sqroot * sqroot
    where sqroot = floor $ sqrt (fromIntegral n::Double)

minSolns :: (Integral a) => [(a,a)]
minSolns = map minSoln nonSquares
    where nonSquares = filter (not . isSquare) [1..]

problem_66 n = last $ sort solns
    where solns = takeWhile (\(x,d) -> d <= n) minSolns
