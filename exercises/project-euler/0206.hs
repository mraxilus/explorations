import Data.List

problem_206 = head [b | b <- expectedResults,
                        any (\n -> 2 == logBase b n) givenSquares ]
    where givenSquares =
            map (\n -> read (intersperse n "1234567890")::Double) ['0'..'9']
          expectedResults = map sqrt givenSquares
