-- problem 3
-- =========
-- the prime factors of 13195 are 5, 7, 13 and 29.
--
-- what is the largest prime factor of the number 600851475143?

import qualified Data.List as L
import qualified Data.Ord as O

-- TODO(mraxilus): implement local factorise method
-- from: http://hackage.haskell.org/package/arithmoi-0.4.0.3
import qualified Math.NumberTheory.Primes.Factorisation as F

solveProblem :: Integer
solveProblem = fst $ L.maximumBy (O.comparing fst) (F.factorise 600851475143)
