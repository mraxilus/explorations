-- problem 7
-- =========
-- by listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
--
-- what is the 10 001st prime number?

-- TODO(mraxilus): implement local sieve
-- from: http://hackage.haskell.org/package/arithmoi-0.4.0.3
import qualified Math.NumberTheory.Primes.Sieve as S

solveProblem :: Integer
solveProblem = S.primes!!10000
