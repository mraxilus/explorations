import Data.List

triangleNums = 1 : 3 : zipWith (+) (tail triangleNums) [3..]

divisors :: Int -> [Int]
divisors n = 1 : filter ((== 0) . rem n) [2..(n `div` 2)]

problem_12 = scanl (withDivLength) (0, 0) (zip [0..] triangleNums) where
    withDivLength (a, b) (c, d) =  if divLength > b then (d, divLength) else (a, b) where
        divLength = length (divisors d)


