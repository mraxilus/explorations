-- problem 6
-- =========
-- the sum of the squares of the first ten natural numbers is,
--   1^2 + 2^2 + ... + 10^2 = 385
--
-- the square of the sum of the first ten natural numbers is,
--   (1 + 2 + ... + 10)2 = 552 = 3025
--
-- hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
--
-- find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

sumOfSquares :: [Int] -> Int
sumOfSquares xs = sum $ map (^2) xs

squareOfSum :: [Int] -> Int
squareOfSum xs = (sum xs) ^ 2

sumSquareDifference :: [Int] -> Int
sumSquareDifference xs = abs (sumOfSquares xs - squareOfSum xs)

solveProblem :: Int
solveProblem = sumSquareDifference [1..100]
