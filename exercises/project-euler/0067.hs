#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 67
-- ==========
-- by starting at the top of the triangle below and moving to adjacent numbers on the row below,
--   the maximum total from top to bottom is 23.
--
-- 3
-- 7 4
-- 2 4 6
-- 8 5 9 3
--
-- that is,
--   3 + 7 + 4 + 9 = 23.
--
-- find the maximum total from top to bottom in triangle.txt (right click and 'save link/target as...'),
--   a 15k text file containing a triangle with one-hundred rows.
--
-- note:
--   this is a much more difficult version of problem 18.
--   it is not possible to try every route to solve this problem,
--     as there are 299 altogether!
--   if you could check one trillion (1012) routes every second it would take over twenty billion years to check them all.
--   there is an efficient algorithm to solve it. ;o)
--
-- insights
-- --------
-- - refer to problem 18.

import qualified ProjectEuler.Functions as F
import qualified System.IO as SIO

main :: IO ()
main = do
  content <- SIO.readFile "0067.txt"
  let rows = lines content
      triangle = map (map (read) . words) rows
      solution = F.maxPathSum (reverse triangle)
  print solution
