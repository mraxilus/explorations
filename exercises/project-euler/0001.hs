#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 1
-- =========
-- if we list all the natural numbers below 10 that are multiples of 3 or 5, 
--   we get 3, 5, 6 and 9. 
-- the sum of these multiples is 23.
--
-- find the sum of all the multiples of 3 or 5 below 1000.

module Main where

main :: IO ()
main = do
  let multiples = takeWhile (< 1000) (multiplesOf 3 5)
  let solution = sum multiples
  print solution

multiplesOf :: (Integral a) => a -> a -> [a]
multiplesOf x y = [ n | n <- [1..], n `mod` x == 0 || n `mod` y == 0 ]
