#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- question 15
-- ===========
-- starting in the top left corner of a 2×2 grid,
--   and only being able to move to the right and down,
--   there are exactly 6 routes to the bottom right corner.
--
-- how many such routes are there through a 20×20 grid?

-- insights
-- --------
-- possible routes for each vertex to bottom right corner coincide with pascal's triangle.
-- notice that the diagonal line matches with the centre of even rows of the triangle.
-- thus, for even sized square grids the total possible routes is 2s choose s.

module Main where

import qualified ProjectEuler.Functions as F

main :: IO ()
main = do
  let solution = 40 `F.choose` 20
  print solution

