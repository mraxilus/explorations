#!/usr/bin/env runhaskell
-- this file is part of project-euler which is released under the mit license.
-- go to http://opensource.org/licenses/mit for full details.
--
-- problem 100
-- ===========
-- if a box contains twenty-one coloured discs,
--   composed of fifteen blue discs and six red discs,
--   and two discs were taken at random,
--   it can be seen that the probability of taking two blue discs,
--   p(bb) = (15/21)×(14/20) = 1/2.
--
-- the next such arrangement,
--   for which there is exactly 50% chance of taking two blue discs at random,
--   is a box containing eighty-five blue discs and thirty-five red discs.
--
-- by finding the first arrangement to contain over 10^12 = 1,000,000,000,000 discs in total,
--   determine the number of blue discs that the box would contain.

module Main where

main :: IO ()
main = do
  let solution =  head [(ap, b, r) | b <- [1..],
                                 r <- [b/3..b/2], 
                                 let total = b + r,
                                 total > (10^12),
                                 let p = (b/total) * ((b - 1)/(total - 1)),
                                 let ap =  (fromInteger $ round $ p * (10^5)) / (10.0^^5),
                                 ap == 1/2]
  print solution

