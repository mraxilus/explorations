let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = [
    pkgs.hello
    pkgs.jupyter
    pkgs.python3
    pkgs.python37Packages.pip
  ];
}
