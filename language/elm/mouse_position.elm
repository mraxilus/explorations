import Html exposing (..)
import Mouse exposing (..)

main =
  Html.program
  { init = init
  , view = view
  , update = update
  , subscriptions = subscriptions
  }

type alias Model =
  { x : Int
  , y : Int
  }

init : (Model, Cmd Msg)
init = (Model 0 0, Cmd.none)

type Msg = Position Int Int

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Position x y ->
      ({model | x=x, y=y}, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions model =
  Mouse.moves (\{x, y} -> Position x y)

view : Model -> Html a
view model =
  Html.text (toString model)
