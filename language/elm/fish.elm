import Html exposing (..)
import Html.Attributes exposing (..)
import AnimationFrame

main =
  Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

-- Model

type alias Model =
  { entity : Fish
  }

type alias Fish =
  { x : Float
  , y : Float
  , speed : Float  -- pixels per millisecond
  , angle : Float  -- radians
  }

init : (Model, Cmd Msg)
init = (Model (Fish 200 200 0.1 1.5), Cmd.none)


-- Update

type Msg
  = Tick Float

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Tick delta ->
      step delta model

step : Float -> Model -> (Model, Cmd Msg)
step delta model =
  let
    x_ = model.entity.x + delta * model.entity.speed * cos model.entity.angle
    y_ = model.entity.y + delta * model.entity.speed * sin model.entity.angle
    old = model.entity
    entity_ =
      {old | x=x_, y=y_}
  in
    ({model | entity=entity_}, Cmd.none)


subscriptions : Model -> Sub Msg
subscriptions model =
  AnimationFrame.diffs Tick


-- View
(=>) = (,)

view : Model -> Html a
view model =
  Html.div
    [ style
          [ "background-color" => "#3C8D2F"
          , "width" => "10px"
          , "height" => "10px"
          , "border-radius" => "5px"
          , "position" => "absolute"
          , "left" => px model.entity.x
          , "top" => px model.entity.y
          , "display" => "flex"
          ]
    ]
    []

px : Float -> String
px number =
  toString number ++ "px"
