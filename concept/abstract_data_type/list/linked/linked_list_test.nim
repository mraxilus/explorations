import unittest, quicktest, linked_list, random
randomize()

suite "linked list":
  quicktest "create linked list" do():
    var list = create_linked_list[int]()
    check(list.head == nil)
    check(list.length == 0)
    destroy_linked_list(list)

  quicktest "destroy linked list if null" do():
    var list: ptr LinkedList[int] = nil
    destroy_linked_list(list)

  quicktest "destroy linked list if valid" do():
    var list = create_linked_list[int]()
    destroy_linked_list(list)

  quicktest "create node" do(value: int()):
    var node = create_node[int](value)
    check(node.value == value)
    check(node.next == nil)
    destroy_node(node)

  quicktest "destroy node if null" do():
    var node: ptr Node[int] = nil
    destroy_node(node)

  quicktest "destroy node if valid" do(value: int()):
    var node = create_node[int](value)
    destroy_node(node)

  quicktest "insert on null" do(value: int()):
    var list: ptr LinkedList[int] = nil
    list.insert(value)

  quicktest "insert on empty" do(value: int()):
    var list = create_linked_list[int]()
    list.insert(value)
    check(list.length == 1)
    check(list.head.value == value)
    check(list.head.next == nil)
    destroy_linked_list(list)

  quicktest "insert on non-empty" do(values: seq[int()]):
    if len(values) < 2:
      break

    var list = create_linked_list[int]()
    for value in values:
      list.insert(value)
    check(list.length == len(values))

    var node = list.head
    for i in countdown(len(values)-1, 0):
      check(node.value == values[i])
      node = node.next
    check(node == nil)

    destroy_linked_list(list)

  quicktest "search on null" do(value: int()):
    var list: ptr LinkedList[int] = nil
    var node = list.search(value)
    check(node == nil)

  quicktest "search on empty" do(value: int()):
    var list = create_linked_list[int]()
    var node = list.search(value)
    check(node == nil)
    destroy_linked_list(list)

  quicktest "search on valid non-empty" do(values: seq[int()]):
    if len(values) < 2:
      break

    var list = create_linked_list[int]()
    for value in values:
      list.insert(value)

    var search_value = values[rand(len(values)-1)]
    var node = list.search(search_value)
    check(node.value == search_value)

    destroy_linked_list(list)

  quicktest "search on invalid non-empty" do(values: seq[int()], search_value: int()):
    if len(values) < 1 or search_value in values:
      break

    var list = create_linked_list[int]()
    for value in values:
      list.insert(value)

    var node = list.search(search_value)
    check(node == nil)

    destroy_linked_list(list)

  quicktest "insert after into null" do(value: int()):
    var node: ptr Node[int] = nil
    expect AccessViolationError:
      node.insert_after(value)
    destroy_node(node)

  quicktest "insert after into end of node" do(value0: int(), value1: int()):
    var node = create_node(value0)
    node.insert_after(value1)
    check(node.next.value == value1)
    check(node.next.next == nil)
    destroy_node(node)

  quicktest "insert after into middle of nodes" do(value0: int(), value1: int(), value2: int()):
    var node0 = create_node(value0)
    var node1 = create_node(value1)
    node0.next = node1
    node0.insert_after(value2)
    check(node0.next.value == value2)
    check(node0.next.next == node1)
    destroy_node(node0)
    destroy_node(node1)

  quicktest "remove on null" do():
    var
      list: ptr LinkedList[int] = nil
      node = list.remove()
    check(node == nil)
    destroy_linked_list(list)

  quicktest "remove on emtpy" do():
    var
      list = create_linked_list[int]()
      node = list.remove()
    check(node == nil)

  quicktest "remove on non-empty" do(values: seq[int()]):
    var list = create_linked_list[int]()
    for value in values:
      list.insert(value)
    for i in countdown(len(values) - 1, 0):
      var node = list.remove()
      check(node.value == values[i])
      check(list.length == i)
    check(list.head == nil)
