

type Node*[T] = object
  value*: T
  next*: ptr Node[T]


type LinkedList*[T] = object
  head*: ptr Node[T]
  length*: Natural



proc create_node*[T](value: T): ptr Node[T] =
  result = cast[ptr Node[T]](alloc(sizeof(Node[T])))
  result.value = value
  result.next = nil


proc destroy_node*[T](node: ptr Node[T]) =
  if node != nil:
    dealloc(node)


proc create_linked_list*[T](): ptr LinkedList[T] =
  result = cast[ptr LinkedList[T]](alloc(sizeof(LinkedList[T])))
  result.head = nil
  result.length = 0


proc destroy_linked_list*[T](list: ptr LinkedList[T]) =
  if list == nil:
    return

  if list.head == nil:
    dealloc(list)
    return

  var current_node = list.head
  var nodes = @[current_node]
  while current_node.next != nil:
    nodes.add(current_node.next)
    current_node = current_node.next

  for i, node in nodes:
    destroy_node(node)
  dealloc(list)


proc insert*[T](list: ptr LinkedList[T], value: T) =
  if list == nil:
    return

  var node = create_node[T](value)

  if list.length > 0:
    node.next = list.head

  list.head = node
  list.length += 1


proc search*[T](list: ptr LinkedList[T], value: T): ptr Node[T] =
  if list == nil or list.length == 0:
    return nil

  var node = list.head
  for _ in 0..<list.length:
    if node.value == value:
      return node

    node = node.next

  return node


proc insert_after*[T](node: ptr Node[T], value: T) =
  if node == nil:
    raise newException(AccessViolationError, "attempt to insert after nil node.")

  var node_new = create_node(value)
  node_new.next = node.next
  node.next = node_new


proc remove*[T](list: ptr LinkedList[T]): ptr Node[T] =
  if list == nil or list.length == 0:
    return nil

  result = list.head
  list.head = list.head.next
  list.length -= 1


#remove_after[T](var node: Node[T]): T =
