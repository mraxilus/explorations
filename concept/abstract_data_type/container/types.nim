import concepts

type
  List*[T] = array[0, T]  ## Alias for array of arbitrary size.

  ## Data and information required to implement stack.
  StackAsArray*[T] = object
    capacity*: Positive ## Capacity of stack.
    head*: int         ## Relative position of head of stack.
    data*: ptr List[T] ## Underlying array for storing stack elements.
  StackStaticAsArray*[N: static Positive, T] = object
    capacity*: Positive = N
    head*: int
    data*: ptr List[T]


