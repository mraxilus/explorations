import unittest, stack

let values = @[1, 2, 3, 4]

suite "stack":
  test "construct":
    let capacity = 2
    var stack = create_stack[int](capacity)
    check(stack.head == -1)
    check(stack.capacity == capacity)
    check(stack.data != nil)
    dealloc stack.data


  test "destruct":
    var stack = create_stack[int]()
    check(stack.data != nil)
    destroy_stack(stack)
    check(stack.data == nil)


  test "push under capacity":
    var values = @[1, 2, 3, 4]
    var stack = create_stack[int](capacity=len(values))
    for i in 0..<len(values):
      stack.push(values[i])
    check(stack.capacity == len(values))
    for i in 0..<len(values):
      check(stack.data[][i] == values[i])
    destroy_stack(stack)


  test "push over capacity":
    var
      stack = create_stack[int](capacity=2)
    for i in 0..<len(values):
      stack.push(values[i])
    check(stack.capacity >= len(values))
    for i in 0..<len(values):
      check(stack.data[][i] == values[i])
    destroy_stack(stack)


  test "peek empty":
    var stack = create_stack[int]()
    expect IndexDefect:
      discard stack.peek()
    destroy_stack(stack)


  test "peek after push":
    var stack = create_stack[int]()
    for i in 0..<len(values):
      stack.push(values[i])
      check(stack.peek() == values[i])
      check(stack.peek() == values[i])  # Peek shouldn't alter stack.
    destroy_stack(stack)


  test "peek after pop":
    var stack = create_stack[int]()
    for i in 0..<len(values):
      stack.push(values[i])
      if i > 0:
        discard stack.pop()
        check(stack.peek() == values[i - 1])
        stack.push(values[i])
    destroy_stack(stack)


  test "pop empty":
    var stack = create_stack[int]()
    expect IndexDefect:
      discard stack.pop()
    destroy_stack(stack)

  test "pop after push":
    var stack = create_stack[int]()
    for i in 0..<len(values):
      stack.push(values[i])
      if i > 0:
        check(stack.pop() == values[i])
        check(stack.data[][stack.head] == values[i - 1]) # Pop should alter stack.
        stack.push(values[i])
    destroy_stack(stack)


  test "size empty":
    var stack = create_stack[int]()
    check(stack.size() == 0)
    destroy_stack(stack)


  test "size after push":
    var stack = create_stack[int]()
    for i in 0..<len(values):
      stack.push(values[i])
      check(stack.size == i + 1)
    destroy_stack(stack)


  test "size after pop":
    var stack = create_stack[int]()
    for i in 0..<len(values):
      stack.push(values[i])
      if i > 0:
        discard stack.pop()
        check(stack.size == i)
        stack.push(values[i])
    destroy_stack(stack)
