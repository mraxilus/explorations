## Implementation of the stack data structure.

type
  List*[T] = array[0, T]  ## Alias for array of arbitrary size.
  Stack*[T] = object
    ## Data and information required to implement stack.
    capacity*: Positive ## Capacity of stack.
    head*: int         ## Relative position of head of stack.
    data*: ptr List[T] ## Underlying array for storing stack elements.


proc `[]`[T](stack: Stack[T], i: Natural): T =
  ## Retrieve element from stack manually without dereferencing.
  if i >= stack.capacity:
    raise newException(IndexDefect, "request for index out of bounds.")
  return stack.data[][int(i)]


proc `[]=`[T](stack: var Stack[T], i: Natural, item: T) =
  ## Assign element to stack manually without dereferencing.
  if i >= stack.capacity:
    raise newException(IndexDefect, "request to set out of bounds index.")
  stack.data[][int(i)] = item


proc create_stack*[T](capacity: Positive = 2): Stack[T] =
  ## Construct new stack with given initial capacity.
  var data = cast[ptr List[T]](alloc(capacity * sizeof(T)))
  return Stack[T](capacity: capacity, head: -1, data: data)


proc destroy_stack*(stack: var Stack) =
  ## Deallocate memory allocated for stack.
  if stack.data != nil:
    dealloc stack.data
    stack.data = nil


proc push*[T](stack: var Stack[T], item: T) =
  ## Put element onto head stack.

  # Double capacity if stack is full.
  if stack.head == stack.capacity - 1:
    var
      new_capacity = stack.capacity * 2
      new_data = cast[ptr List[T]](alloc(new_capacity * sizeof(T)))
    copyMem(new_data, stack.data, stack.capacity * sizeof(T))
    dealloc stack.data
    stack.data = new_data
    stack.capacity = new_capacity

  stack.head += 1
  stack[stack.head] = item


proc size*(stack: var Stack): Natural =
  ## Obtain size of stack.
  return stack.head + 1


proc peek*[T](stack: Stack[T]): T =
  ## Examine element at head of stack.
  if stack.head < 0:
    raise newException(IndexDefect, "inspect on empty stack.")
  result = stack.data[stack.head]


proc pop*[T](stack: var Stack[T]): T =
  ## Examine and remove element at head of stack.
  if stack.head < 0:
    raise newException(IndexDefect, "attempt to pop from empty stack.")
  result = stack[stack.head]
  stack.head -= 1
