import glob

# server

text_files = glob.glob('book/alice_in_wonderland/*.txt')

def file_contents(file_name):
    handle = open(file_name)
    try:
        return handle.read()
    finally:
        handle.close()

source = dict((file_name, file_contents(file_name))
              for file_name in text_files)

def final(word, result):  # i.e. ('alice', ('alice', 3))
    print(result)

# client

def mapfn(file_name, contents):  # i.e. ('alice_in_wonderland.txt', 'alice did stuff...')
    for line in contents.splitlines():
        for word in line.split():
            yield word.lower()[0], 1


def reducefn(word, occurances):  # i.e ('alice', [1, 1, 1])
    return word, len(occurances)
