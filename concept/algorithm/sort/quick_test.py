from hypothesis import assume, given, note, strategies
import quick
import unittest


@strategies.composite
def get_valid_list_and_range(draw, min_size=2, max_size=None):
    xs = draw(strategies.lists(
        strategies.integers(),
        min_size=min_size,
        max_size=max_size,
    ))
    left = draw(strategies.integers(min_value=0, max_value=len(xs) - 2))
    right = draw(strategies.integers(
        min_value=left + 1,
        max_value=len(xs) - 1,
    ))
    return xs, left, right


@strategies.composite
def get_invalid_list_and_range(draw):
    xs = draw(strategies.lists(strategies.integers()))
    left = draw(strategies.integers())
    right = draw(strategies.integers())
    assume(
        left > right  # left index before right.
        or left < 0 or left >= len(xs)  # left within bounds.
        or right < 0 or right >= len(xs)  # right within bounds.
        or len(xs) == 0  # list partitionable.
    )
    return xs, left, right


class QuickTest(unittest.TestCase):
    @given(get_invalid_list_and_range())
    def test_partition_parameters(self, parameters):
        xs, left, right = parameters
        pivot_index = quick.partition(xs, left, right)
        note('pivot index: {0}'.format(pivot_index))
        assert pivot_index == None


    @given(strategies.lists(strategies.integers(), min_size=1, max_size=1))
    def test_partition_one(self, xs):
        pivot_index = quick.partition(xs, 0, len(xs) - 1)
        note('pivot index: {0}'.format(pivot_index))
        assert pivot_index == 0


    @given(get_valid_list_and_range(min_size=2, max_size=2))
    def test_partition_two(self, parameters):
        xs, left, right = parameters
        pivot_index = quick.partition(xs, left, right)
        note('pivot index: {0}'.format(pivot_index))
        assert xs == sorted(xs)
        assert pivot_index == 1


    @given(get_valid_list_and_range(min_size=3))
    def test_partition_many(self, parameters):
        xs, left, right = parameters
        pivot = xs[-1]
        note('pivot: {0}'.format(pivot))
        pivot_index = quick.partition(xs, 0, len(xs) - 1)
        left, right = xs[:pivot_index], xs[pivot_index:]
        note('partitions: {0}, {1}'.format(left, right))
        if len(left) > 0:
            assert max(left) <= pivot
        if len(right) > 0:
            assert min(right) >= pivot


    @given(strategies.lists(strategies.integers()))
    def test_sort_quick_integers(self, xs):
        expected = sorted(xs)
        note('expected: {0}'.format(expected))
        result = quick.sort_by_quick(xs)
        note('result: {0}'.format(result))
        assert expected == result


    @given(strategies.lists(strategies.floats(allow_nan=False)))
    def test_sort_quick_floats(self, xs):
        expected = sorted(xs)
        note('expected: {0}'.format(expected))
        result = quick.sort_by_quick(xs)
        note('result: {0}'.format(result))
        assert expected == result


if __name__ == '__main__':
    unittest.main()
