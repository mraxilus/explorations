

def sort_with_selection(xs: list) -> list:
    sorted_position = 0
    while sorted_position != len(xs) - 1:
        min_position = find_minimum(xs[sorted_position:]) + sorted_position
        new_min_value = xs[min_position]
        front_value = xs[sorted_position]
        xs[min_position] = front_value
        xs[sorted_position] = new_min_value
        sorted_position += 1
    return xs


def find_minimum(xs: list) -> int:
    min_position = 0
    for i in range(len(xs)):
        if xs[i] < xs[min_position]:
            min_position = i
    return min_position


unsorted_list = [12, 5, 2, -1, 7, 13, 15, 1, 8, 1]

print(sort_with_selection(unsorted_list)) # [-1, 1, 1, 2, 5, 7, 8, 12, 13, 15]
