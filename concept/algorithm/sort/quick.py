

def sort_by_quick(xs):
    '''Sort a list using the quicksort algorithm.'''
    def sort_quick(xs, start, end):
        '''Sort part of a list using the quicksort algorithm.'''
        pivot_index = partition(xs, start, end)
        if pivot_index is None:
            return None

        # sort left half.
        sort_quick(xs, start, pivot_index - 1)

        # sort right half preventing partition duplication.
        if pivot_index == start:
            sort_quick(xs, pivot_index + 1, end)
        else:
            sort_quick(xs, pivot_index, end)


    # sort only if possible to be unsorted.
    if len(xs) > 1:
        sort_quick(xs, 0, len(xs) - 1)

    return xs


def partition(xs, start, end):
    '''Partition part of a list into those less and greater than the last element.'''
    # validate arguments.
    if (start < 0 or start >= len(xs)  # start within bounds.
        or end < 0 or end >= len(xs)  # end within bounds.
        or start > end  # start index before end.
       ):
        return None
    range_size = abs(start - end) + 1

    # skip swaps for small subset.
    if range_size == 1:
        return start

    # perform simple swap for two elements.
    if range_size == 2:
        if xs[start] > xs[end]:
            xs[start], xs[end] = xs[end], xs[start]
        return end

    # perform partitioning via single swap pointer.
    swap_point = start
    pivot = xs[end]
    for i in range(start, end):
        if xs[i] < pivot:
            xs[i], xs[swap_point] = xs[swap_point], xs[i]
            swap_point += 1

    # swap pivot into correct location.
    xs[end], xs[swap_point] = xs[swap_point], xs[end]

    return swap_point
