def sort_by_merge(xs):
    if len(xs) <= 1:
        return xs

    split = len(xs) // 2
    left = sort_by_merge(xs[:split])
    right = sort_by_merge(xs[split:])

    result = []
    left_index, right_index = 0, 0
    while left_index < len(left) or right_index < len(right):
        if left_index >= len(left):
            result.append(right[right_index])
            right_index += 1
            continue

        if right_index >= len(right):
            result.append(left[left_index])
            left_index += 1
            continue

        if left[left_index] <= right[right_index]:
            result.append(left[left_index])
            left_index += 1
        else:
            result.append(right[right_index])
            right_index += 1
    return result


unsorted_list = [12, 5, 2, -1, 7, 13, 15, 1, 8, 1]
print(sort_by_merge(unsorted_list))
