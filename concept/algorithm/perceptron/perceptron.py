#!/usr/bin/env python3

import numpy

# Setup training data.
X = [
    numpy.array([-1, 1]),
    numpy.array([0, -1]),
    numpy.array([10, 1]),
]
ys = numpy.array([1, -1, 1])

# Setup model parameters.
ws = numpy.array([0, 0])
b = 0



def main():
    global X, ys, ws, b 

    # Train model until convergence.
    errors = len(X)
    epoch = 0
    while errors > 0:
        errors = len(X)

        # Run through a training epoch.
        for i, xs in enumerate(X):
            y_hat = make_predicion(xs, ws, b)
            if y_hat == ys[i]:
                errors -= 1
            else:
                ws = get_weight_update(ws, xs, ys[i])
                b = get_bias_update(b, ys[i])
            print('epoch: {}, sample: {}, errors: {}, bias: {}, weights: {}'
                    .format(epoch, i, errors, b, ws))

        epoch += 1


def make_predicion(xs, ws, b):
    '''Make a perception prediction for a given input.'''
    return perform_activation(xs.dot(ws) + b)


def perform_activation(y):
    '''Perform a Heaviside step operation.'''
    if y == 0:
        return 0
    return 1 if y > 0 else -1


def get_weight_update(ws, xs, y):
    '''Calculate and return updated weights for a misclassified input.'''
    return ws + (xs * y)


def get_bias_update(b, y):
    '''Calculate and return updated bias for a misclassified input.'''
    return b + y


if __name__ == '__main__':
    main()

