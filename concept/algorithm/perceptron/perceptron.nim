import future


proc predict(w, x, b: float): float {. noSideEffect .} =
  return w * x + b


proc predict(w, x: seq[float], b: float): float {. noSideEffect .} =
  var ŷ: float = 0
  for i in 0..<x.len:
    ŷ += predict(w[i], x[i], 0)
  return ŷ + b


proc perform_perceptron_rule(η, y, ŷ, x: float): float {. noSideEffect .} = 
  return η * (y - ŷ) * x


proc threshold_value(x, threshold: float): int {. noSideEffect .} =
  var value = 0
  if x > threshold:
    value = 1
  else:
    value = 0
  return value



let 
  b = 1.0
  xs = @[@[0.0, 0.0], @[0.0, 1.0], @[1.0, 0.0], @[1.0, 1.0]]
  ys = @[0.0, 0.0, 0.0, 1.0]
var 
  w = @[0.2, 0.2]
  threshold = 1.3

for i in 0..<xs.len:
  echo ys[i], " ", threshold_value(predict(w, xs[i], b), threshold)



