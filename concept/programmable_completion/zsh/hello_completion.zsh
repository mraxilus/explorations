#! /bin/zsh

fpath=($(readlink --canonicalize "${0}" | xargs dirname) ${fpath})
autoload -U compinit
compinit -u
