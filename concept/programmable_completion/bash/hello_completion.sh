#! /bin/bash

_hello_completion() {
  local cur prev opts base
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD - 1]}"
  opts="one two three"
  case "${prev}" in
    one)
      opts="a b c" ;;
    two)
      opts="d e f" ;;
    three)
      opts="g h i" ;;
    a|b|c|d|e|f|g|h|i)
      opts="" ;;
  esac
  COMPREPLY=($(compgen -W "${opts}" -- "${cur}"))
}

complete -F _hello_completion hello_completion

