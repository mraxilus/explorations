package main

import "fmt"

func main() {
  source := []string{
    "",
    "  // print header.",
    "  fmt.Println(\"package main\\n\");",
    "  fmt.Println(\"import \\\"fmt\\\"\\n\");",
    "  fmt.Println(\"func main() {\");",
    "",
    "  // print source string array.",
    "  fmt.Println(\"  source := []string{\");",
    "  for _, line := range source {",
    "    fmt.Printf(\"    %q,\\n\", line)",
    "  }",
    "  fmt.Println(\"  }\");",
    "",
    "  // print core source code.",
    "  for _, line := range source {",
    "    fmt.Println(line)",
    "  }",
    "",
    "  // print footer.",
    "  fmt.Println(\"}\");",
  }

  // print header.
  fmt.Println("package main\n");
  fmt.Println("import \"fmt\"\n");
  fmt.Println("func main() {");

  // print source string array.
  fmt.Println("  source := []string{");
  for _, line := range source {
    fmt.Printf("    %q,\n", line)
  }
  fmt.Println("  }");

  // print core source code.
  for _, line := range source {
    fmt.Println(line)
  }

  // print footer.
  fmt.Println("}");
}
