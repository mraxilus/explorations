[Explorations][website]
===

_Explorations of various technologies._


Purpose
---
This repository is just a place to learn art, concepts, languages, libraries, and tools. 
It is for personal understanding, but it should also serve as a reference for others.


License
---
I have licensed this project under [CC BY-NC 4.0][license].

You may negociate a waiver for the non-commercial restriction.
Please contact me at legal@projectaxil.us to do so.


[website]: https://emmanuel.website/
[license]: https://creativecommons.org/licenses/by-nc/4.0/
