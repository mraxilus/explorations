#!/usr/bin/env python3

from math import pi, sin, cos
from pyglet.gl import *
import pyglet
import pymunk
import random

window = pyglet.window.Window(width=1280, height=720)
fps = 60.0
clock = pyglet.clock.Clock(fps_limit=fps)
space = pymunk.Space()
space.gravity = (0.0, -100.0)

balls = []
ticks_to_next_ball = 10

@window.event
def on_draw():
  global ticks_to_next_ball
  
  glClear(GL_COLOR_BUFFER_BIT)
  glColor3f(1, 1, 1)

  window.clear()
  space.step(clock.tick()) 
  ticks_to_next_ball -= 1

  if ticks_to_next_ball <= 0:
    ticks_to_next_ball = fps
    ball_shape = add_ball(space)
    balls.append(ball_shape)

  for ball in balls:
    draw_ball(ball)


def add_ball(space):
  mass = 1
  radius = 14
  inertia = pymunk.moment_for_circle(mass, 0, radius)
  body = pymunk.Body(mass, inertia)
  x = random.randint((window.width // 2) - 300, (window.width // 2) + 300)
  body.position = x, (window.height // 2) + 200
  shape = pymunk.Circle(body, radius)
  space.add(body, shape)
  return shape

def draw_ball(ball):
  print('draw', ball.body.position.x, ball.body.position.y)
  circle(ball.body.position.x, ball.body.position.y, ball.radius)
  
def circle(x, y, radius):
    """
    We want a pixel perfect circle. To get one,
    we have to approximate it densely with triangles.
    Each triangle thinner than a pixel is enough
    to do it. Sin and cosine are calculated once
    and then used repeatedly to rotate the vector.
    I dropped 10 iterations intentionally for fun.
    """
    iterations = int(2*radius*pi)
    s = sin(2*pi / iterations)
    c = cos(2*pi / iterations)

    dx, dy = radius, 0

    glBegin(GL_TRIANGLE_FAN)
    glVertex2f(x, y)
    for i in range(iterations+1 - 10):
        glVertex2f(x+dx, y+dy)
        dx, dy = (dx*c - dy*s), (dy*c + dx*s)
    glEnd()

if __name__ == '__main__':
  pyglet.app.run()

