#!/usr/bin/env python3

import asyncio
import discord
import discord.ext.commands
import time
import twitch.api.v3

BOT = discord.ext.commands.Bot(command_prefix='*') 

# Configure settings.
DISCORD_CHANNEL = '267517506330034187'
TOKEN = ''
TWITCH_CHANNEL = 'gamesdonequick'


async def check_game_status():
    # Wait for bot to login.
    await BOT.wait_until_ready()

    # Create reference to discord channel to send messages on.
    discord_channel = discord.Object(id=DISCORD_CHANNEL)

    # Continually update the current game being played.
    current_game = None
    while True:
        try:
            # Wait for next poll.
            await asyncio.sleep(5) 

            # Get channel data.
            twitch_channel = twitch.api.v3.channels.by_name(TWITCH_CHANNEL)

            # Wait if channel not live.
            if 'game' not in twitch_channel:
                continue

            # Check if game state has changed since last poll.
            game = twitch_channel['game']
            if game == current_game:
                continue

            # Update current game and notify users.
            current_game = game
            await BOT.send_message(discord_channel,
                                   '**is now running {0}.**'.format(current_game))
        except Exception as exception:
            print(exception)


# Setup and start the bot.
BOT.loop.create_task(check_game_status())
BOT.run(TOKEN)
