WIDTH = 1280
HEIGHT = 720
UNIT = WIDTH/16

function love.load()
  -- Setup physics.
  love.physics.setMeter(UNIT)
  world = love.physics.newWorld(0, 9.81*UNIT, true)

  -- Create the ground.
  objects = {}
  objects.ground = {}
  objects.ground.body = love.physics.newBody(world, WIDTH/2, HEIGHT-(UNIT/2))
  objects.ground.shape = love.physics.newRectangleShape(WIDTH, UNIT)
  objects.ground.fixture = love.physics.newFixture(objects.ground.body, 
    objects.ground.shape)
  
  -- Create the player.
  objects.ball = {}
  objects.ball.body = love.physics.newBody(world, WIDTH/2, HEIGHT-(UNIT/2), "dynamic")
  objects.ball.shape = love.physics.newRectangleShape(UNIT, UNIT)
  objects.ball.fixture = love.physics.newFixture(objects.ball.body, 
    objects.ball.shape, 1)

  -- Setup window.
  love.graphics.setBackgroundColor(119, 119, 119)
  love.window.setMode(WIDTH, HEIGHT, {vsync=false})
end

function love.update(dt)
  world:update(dt)

  -- Allow for horizontal movement.
  if love.keyboard.isDown("right") then
    objects.ball.body:setLinearVelocity(UNIT*12, 0)
  elseif love.keyboard.isDown("left") then
    objects.ball.body:setLinearVelocity(-UNIT*12, 0)
  else
    objects.ball.body:setLinearVelocity(0, 0)
  end
end

function love.draw()
  -- Draw object fills.
  love.graphics.setColor(221, 221, 221)
  love.graphics.print(tostring(love.timer.getFPS()).." fps", 10, 10)
  love.graphics.polygon("fill", 
    objects.ground.body:getWorldPoints(objects.ground.shape:getPoints()))
  love.graphics.polygon("fill", 
    objects.ball.body:getWorldPoints(objects.ball.shape:getPoints()))

  -- Draw object outlines.
  love.graphics.setColor(45, 45, 45)
  love.graphics.polygon("line", 
    objects.ground.body:getWorldPoints(objects.ground.shape:getPoints()))
  love.graphics.polygon("line", 
    objects.ball.body:getWorldPoints(objects.ball.shape:getPoints()))
end

