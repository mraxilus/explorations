import sdl2
import opengl

# Initialise OpenGL.
discard init(INIT_EVERYTHING)
discard glSetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE)
discard glSetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4)
discard glSetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0)
discard glSetAttribute(SDL_GL_STENCIL_SIZE, 8)
loadExtensions()

# Create window, context, and event handler.
var 
  window = createWindow("OpenGL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600,
                        SDL_WINDOW_OPENGL)
  context = glCreateContext(window)
  event = defaultEvent

# Setup vertices.
var vertices: array[32, GLdouble] = [
  #                                 Texture
  # Position   Color                Coordinate
  -0.5,  0.5,  1.0, 0.0, 0.0, 1.0,  0.0, 0.0,  # Top-left.
   0.5,  0.5,  0.0, 1.0, 0.0, 1.0,  1.0, 0.0,  # Top-right.
   0.5, -0.5,  0.0, 0.0, 1.0, 1.0,  1.0, 1.0,  # Bottom-right. 
  -0.5, -0.5,  1.0, 1.0, 1.0, 0.1,  0.0, 1.0,  # Bottom-left.
]

# Setup elements.
var elements: array[6, GLuint] = [
  0.GLuint, 1.GLuint, 2.GLuint,
  0.GLuint, 2.GLuint, 3.GLuint,
]

# Setup vertex array.
var vertex_array: GLuint
glGenVertexArrays(1, addr(vertex_array))
glBindVertexArray(vertex_array)

# Setup vertex buffer.
var vertex_buffer: GLuint 
glGenBuffers(1, addr(vertex_buffer))
glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer)
glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), addr(vertices), GL_STATIC_DRAW)

# Setup element buffer.
var element_buffer: GLuint
glGenBuffers(1, addr(element_buffer))
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer)
glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), addr(elements), GL_STATIC_DRAW)

# Setup texture image.
var 
  texture: GLuint
  texture_image: array[12, GLfloat] = [
    0.0.GLfloat, 0.0.GLfloat, 0.0.GLfloat,  1.0.GLfloat, 1.0.GLfloat, 1.0.GLfloat,
    1.0.GLfloat, 1.0.GLfloat, 1.0.GLfloat,  0.0.GLfloat, 0.0.GLfloat, 0.0.GLfloat,
  ]
glGenTextures(1, addr(texture))
glBindTexture(GL_TEXTURE_2D, texture)
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB.GLint, 2, 2, 0, GL_RGB, cGL_FLOAT, addr(texture_image))

# Set texture settings.
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
glGenerateMipmap(GL_TEXTURE_2D)

# Define vertex and fragment shaders.
var
  vertex_shader_source: cstringArray = allocCStringArray([
    """
    #version 400
  
    in vec2 position;
    in vec2 texture_coordinates;
    in vec4 color;

    out vec2 vertex_texture_coordinates;
    out vec4 vertex_color;
  
    void main() {
      gl_Position = vec4(position, 0.0, 1.0);
      vertex_color = color;
      vertex_texture_coordinates = texture_coordinates;
    }
    """
  ])
  fragment_shader_source: cstringArray = allocCStringArray([
    """
    #version 400

    in vec2 vertex_texture_coordinates;
    in vec4 vertex_color;
  
    out vec4 outColor;

    uniform sampler2D texture_sampler;
  
    void main() {
      outColor = vertex_color;
        outColor = texture(texture_sampler, vertex_texture_coordinates) * vertex_color;
    }
    """
  ])

# Setup shaders.
var 
  vertex_shader: GLuint = glCreateShader(GL_VERTEX_SHADER)
  fragment_shader: GLuint = glCreateShader(GL_FRAGMENT_SHADER)
  vertex_status: GLint
  fragment_status: GLint
glShaderSource(vertex_shader, 1, vertex_shader_source, nil)
glShaderSource(fragment_shader, 1, fragment_shader_source, nil)
glCompileShader(vertex_shader)
glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, addr(vertex_status))
glCompileShader(fragment_shader)
glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, addr(fragment_status))
if vertex_status != cast[GLint](GL_TRUE) or fragment_status != cast[GLint](GL_TRUE):
    echo("error: shaders did not compile correctly.")

# Create program.
var program: GLuint = glCreateProgram()
glAttachShader(program, vertex_shader)
glAttachShader(program, fragment_shader)
glLinkProgram(program)
glUseProgram(program)

# Setup attributes.
var
  position_attribute: GLuint = cast[GLuint](glGetAttribLocation(program, "position"))
  color_attribute: GLuint = cast[GLuint](glGetAttribLocation(program, "color"))
  texture_attribute: GLuint = cast[GLuint](glGetAttribLocation(program, "texture_coordinates"))
glVertexAttribPointer(position_attribute, 2, cGL_DOUBLE, GL_FALSE, 8*sizeof(GLdouble), nil)
glEnableVertexAttribArray(position_attribute)
glVertexAttribPointer(color_attribute, 4, cGL_DOUBLE, GL_FALSE, 8*sizeof(GLdouble), 
                      cast[pointer](2*sizeof(GLdouble)))
glEnableVertexAttribArray(color_attribute)
glVertexAttribPointer(texture_attribute, 2, cGL_DOUBLE, GL_FALSE, 8*sizeof(GLdouble), 
                      cast[pointer](6*sizeof(GLdouble)))
glEnableVertexAttribArray(texture_attribute)


# Check for errors.
var error_code = glGetError()
if error_code != GL_NO_ERROR:
  echo("error: received error code ", cast[int](error_code))

# Run main game loop.
while true:
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
  glEnable(GL_BLEND)
  glClearColor(0.1, 0.1, 0.1, 1.0)
  glClear(GL_COLOR_BUFFER_BIT)
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nil)
  glSwapWindow(window)
  if pollEvent(event):
    if event.kind == QuitEvent:
        break
    if event.kind == KeyUp and event.key.keysym.sym == K_ESCAPE:
        break

# Cleanup objects.
glDeleteProgram(program)
glDeleteShader(fragment_shader)
glDeleteShader(vertex_shader)
glDeleteBuffers(1, addr(element_buffer))
glDeleteBuffers(1, addr(vertex_buffer))
glDeleteVertexArrays(1, addr(vertex_array))
destroy(window)
sdl2.quit()
