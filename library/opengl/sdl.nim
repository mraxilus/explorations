import sdl2
import opengl

# Initialise OpenGL.
discard init(INIT_EVERYTHING)
discard glSetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE)
discard glSetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3)
discard glSetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2)
discard glSetAttribute(SDL_GL_STENCIL_SIZE, 8)

# Create window.
var 
  window = createWindow("OpenGL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600,
                        SDL_WINDOW_OPENGL)
  context = glCreateContext(window)
  event = defaultEvent

# Load load additional extensions.
loadExtensions()
var vertex_buffer: GLuint 
glGenBuffers(1, addr(vertex_buffer))
echo("extentions loaded: ", vertex_buffer)

while true:
  if pollEvent(event):
    if event.kind == QuitEvent:
        break
    if event.kind == KeyUp and event.key.keysym.sym == K_ESCAPE:
        break
  glSwapWindow(window)

# Cleanup objects.
destroy(window)
sdl2.quit()
