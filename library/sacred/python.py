#!/usr/bin/env python3

import sacred

experiment = sacred.Experiment('command_line')

@experiment.config
def configuration():
    recipient = 'command line'
    message = 'hello {0}!'.format(recipient)

@experiment.main
def main(message):
    print(message)

experiment.run(config_updates={'recipient': 'python script'})
