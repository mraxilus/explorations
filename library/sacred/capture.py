#!/usr/bin/env python3

import sacred

experiment = sacred.Experiment('capturing')

@experiment.config
def configuration():
    x = 'this is'
    z = 'seen!'

@experiment.capture
def concatinate(x, y='frequently', z='never seen!'):
    print(x, y, z)


@experiment.automain
def main():
    concatinate('hello', 'good', 'world!')
    concatinate(y='sometimes')
    concatinate('and this is')
