#!/usr/bin/env python3

import sacred

experiment = sacred.Experiment('command_line')

@experiment.config
def configuration():
    recipient = 'command line'
    message = 'hello {0}!'.format(recipient)

@experiment.automain
def main(message):
    print(message)
