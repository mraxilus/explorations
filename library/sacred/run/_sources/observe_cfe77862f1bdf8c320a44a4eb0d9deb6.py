#!/usr/bin/env python3

import sacred
from sacred.observers import FileStorageObserver

experiment = sacred.Experiment('observe')

@experiment.config
def configuration():
    recipient = 'observer'
    message = 'hello {0}!'.format(recipient)

@experiment.main
def main(message):
    print(message)

experiment.observers.append(
    FileStorageObserver.create('run')
)
experiment.run()
