#!/usr/bin/env python3

import ray

@ray.remote
def add_one(x):
    return x + 1


if __name__ == '__main__':
    ray.init()
    x = add_one.remote(0)
    y = add_one.remote(x)
    z = add_one.remote(y)
    print(ray.get(z))
