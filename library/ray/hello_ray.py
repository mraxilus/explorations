#!/usr/bin/env python3

import ray
import time
from timeit import default_timer as timer


def do_work_serial():
    time.sleep(1)
    return 1


@ray.remote
def do_work_parallel():
    time.sleep(1)
    return 1


if __name__ == '__main__':
    start = timer()
    [do_work_serial() for _ in range(4)]
    end = timer()
    print('serial:', end - start)

    ray.init()
    start = timer()
    ray.get([do_work_parallel.remote() for _ in range(4)])
    end = timer()
    print('parallel:', end - start)
