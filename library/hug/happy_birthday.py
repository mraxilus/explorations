#!/usr/bin/env python3

import hug

@hug.get(examples='name=Tom&age=20')
@hug.local()
@hug.cli()
def say_happy_birthday(name: hug.types.text, age: hug.types.number, hug_timer=3):
    """An example of the Hug API functionality."""
    return {
        'message': 'happy {0} birthday, {1}!'.format(age, name),
        'took': hug_timer,
    }

if __name__ == '__main__':
    say_happy_birthday.interface.cli()
