#!/usr/bin/env python3

import pyglet
from pyglet import gl
from pyglet.window import key

window = pyglet.window.Window(width=1280, height=720, resizable=True)
fps = 120
fps_display = pyglet.window.FPSDisplay(window)
fps_display.label.color = (26, 26, 26, 255)
label = pyglet.text.Label(
    text=".i coi munje",
    font_name="monokai",
    font_size=64,
    color=(230, 230, 230, 255),
    x=int(window.width * 0.5),
    y=int(window.height * 0.66),
    anchor_x='center',
    anchor_y='center')
image = pyglet.image.load('logo.png')
sprite = pyglet.sprite.Sprite(image)
X, Y = (int((window.width - (sprite.width // 2)) * 0.45), int(
    (window.height - (sprite.height // 2)) * 0.25))
sprite.set_position(X, Y)
music = pyglet.media.load('music.wav')


@window.event
def on_draw():
    gl.glEnable(gl.GL_BLEND)
    gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
    gl.glClearColor(0.5, 0.5, 0.5, 1)
    window.clear()
    label.draw()
    sprite.draw()
    fps_display.draw()
    pyglet.clock.tick()


@window.event
def on_key_press(symbol, modifiers):
    if symbol in [key.Q, key.ESCAPE]:
        window.close()


@window.event
def on_resize(width, height):
    label.x = int(width * 0.5)
    label.y = int(height * 0.66)


@window.event
def on_mouse_motion(x, y, dx, dy):
    global X, Y
    X, Y = x - (sprite.width // 2), y - (sprite.height // 2)


def update(dt):
    sprite.x += (X - sprite.x) * dt * 2
    sprite.y += (Y - sprite.y) * dt * 2


pyglet.clock.schedule_interval(update, 1 / fps)
music.play()
pyglet.app.run()
