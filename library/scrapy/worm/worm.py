#!/usr/bin/env python

import csv as c
import re as r


def main():
  with open('worm.csv') as file_csv:
    index = 0
    for row in c.reader(file_csv):
      chapter = {
        'content': row[0],
        'title': row[1],
      }
      save_html(chapter, index)
      index += 1


def save_html(chapter, file_name_prefix=''):
  title = chapter['title']
  content = chapter['content']

  # construct name of file to write to.
  file_name_postfix = extract_title_text(title).lower()
  file_name_postfix = r.sub('[^\w\s\.]', '', file_name_postfix)
  file_name_postfix = r.sub('\s+', '_', file_name_postfix)
  file_name = str(file_name_prefix) + '_' + file_name_postfix  + '.html'

  # construct head.
  head = (
    '<!doctype html>'
    '<html>'
    '<head>'
    '<title>' + extract_title_text(title) + '</title>'
    '</head>'
  )

  # contstruct body.
  body = (
    '<body>'
    '' + title + ''
    '' + content + ''
    '</body>'
    '</html>'
  )

  # create and save file.
  with open('html/' + file_name, 'w') as file_handle:
    file_handle.write(head + body)


def extract_title_text(title):
  match = r.search('.*>(.*)<\/h1>', title)
  return match.group(1) if match else ''


if __name__ == '__main__':
  main()
