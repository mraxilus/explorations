# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy.item as i

class WormItem(i.Item):
  # define the fields for your item here like:
  title = i.Field()
  content = i.Field()
