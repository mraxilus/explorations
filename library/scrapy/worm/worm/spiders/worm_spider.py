import scrapy.contrib.spiders as sp 
import scrapy.contrib.linkextractors.sgml as sg
import scrapy.selector as se
import re as r
import worm.items as i

class WormSpider(sp.CrawlSpider):
  name = 'worm'
  allowed_domains = ['parahumans.wordpress.com']
  start_urls = ['http://parahumans.wordpress.com/2011/06/11/1-1/']
  link_extractor = sg.SgmlLinkExtractor(
    allow=[r'http:\/\/parahumans.wordpress.com\/\d{4}\/\d{2}\/\d{2}\/.+'],
    restrict_xpaths=["//p/a[contains(text(), 'Next Chapter')][1]"]
  )
  rules = [sp.Rule(link_extractor, callback='parse_item', follow=True)]

  def parse_item(self, response):
    # parse title and content.
    selector = se.Selector(response)
    delimeter = ' '
    title = selector.xpath("//h1[@class='entry-title']").extract()[0] + delimeter
    content = selector.xpath('//div/article/div[1]').extract()
    content = delimeter.join(content)

    # construct item.
    item = i.WormItem()
    item['title'] = title.strip()
    item['content'] = r.sub(r'\s+', ' ', content).strip()

    print item

    return [item]
