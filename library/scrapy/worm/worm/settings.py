# Scrapy settings for worm project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'worm'

SPIDER_MODULES = ['worm.spiders']
NEWSPIDER_MODULE = 'worm.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'worm (+http://www.yourdomain.com)'
