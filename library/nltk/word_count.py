#!/usr/bin/env python3
#
# You must follow these instructions to download the required files:
#   http://stackoverflow.com/questions/4867197/failed-loading-english-pickle-with-nltk-data-load/10852888#10852888

import nltk

# Gather raw tokens from text file.
text = ''
with open('words.txt') as handle:
    text = handle.read()
tokens = nltk.word_tokenize(text)

# Clean tokens by omitting non-words.
tokens_clean = []
for token in tokens:
    token = token.lower()
    if token.isalpha():
        tokens_clean.append(token)

frequencies = nltk.FreqDist(tokens_clean)
for token in frequencies:
    print(token, frequencies[token])
