#!/usr/bin/env python3
from hypothesis import given
import hypothesis.strategies as strategies


@given(strategies.integers(), strategies.integers(), strategies.integers())
def are_ints_are_associative(x: int, y: int, z: int) -> bool:
    assert (x + y) + z == x + (y + z)


@given(strategies.lists(strategies.integers()))
def are_lists_reversible(xs: list) -> bool:
    ys = list(xs)
    ys.reverse()
    ys.reverse()
    assert xs == ys


if __name__ == '__main__':
    are_ints_are_associative()
    are_lists_reversible()
