#!/usr/bin/env python3

import load_CIFAR10
import show_CIFAR10
import train_CIFAR10
from torch import nn, autograd, optim
import torch
import torchvision

correct, total = 0, 0
for batch in load_CIFAR10.test_loader:
    images, labels = batch
    outputs = train_CIFAR10.model(autograd.Variable(images))
    _, predictions = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predictions == labels).sum()

print('accuracy on 10,000 test images: {0}'.format(100 * correct / total))


class_correct = list(0.0 for _ in range(10))
class_total = list(0.0 for _ in range(10))
for batch in load_CIFAR10.test_loader:
    images, labels = batch
    outputs = train_CIFAR10.model(autograd.Variable(images))
    _, predictions = torch.max(outputs.data, 1)
    classes = (predictions == labels).squeeze()
    for i in range(4):
        label = labels[i]
        class_correct[label] += classes[i]
        class_total[label] += 1

for i in range(10):
    print('accuracy of {0} class: {1}'.format(load_CIFAR10.classes[i],
            100 * class_correct[i] / class_total[i]))
    

