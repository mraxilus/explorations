#!/usr/bin/env python3

import math
import matplotlib.pyplot as pyplot
import numpy

TAU = 2 * numpy.pi


def generate_sin_wave(sample_rate=100, amplitude=1, cycles=1, phase=0):
    global TAU
    samples = numpy.arange(sample_rate * cycles) / sample_rate
    x = TAU * samples
    y = amplitude * numpy.sin(x + phase)
    return x, y

phases = 2 * TAU * numpy.random.random_sample(1024) - TAU
waves = numpy.array([generate_sin_wave(cycles=1, sample_rate=50, phase=phase)[1] for phase in phases])
numpy.savetxt('waves.txt', waves)

print(waves.shape)
for wave in waves[:3]:
    pyplot.plot(wave)
pyplot.savefig('graph.png', dpi=512)

