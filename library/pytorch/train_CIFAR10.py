#!/usr/bin/env python3

import create_neural_network
import load_CIFAR10
import show_CIFAR10
import os.path
from torch import nn, autograd, optim
import torch

model_path = 'model.pkl'
if not os.path.isfile(model_path):
    model = create_neural_network.Net(3)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)

    for epoch in range(10):
        running_loss = 0.0
        for i, batch in enumerate(load_CIFAR10.train_loader):
            inputs, labels = autograd.Variable(batch[0]), autograd.Variable(batch[1])
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.data[0]
            if i % 2000 == 1999:
                print('epoch: {0}, loss: {1}'.format(epoch, running_loss / 2000))
                running_loss = 0.0
    print('finished')
    torch.save(model, model_path)

model = torch.load(model_path)
