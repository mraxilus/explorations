#!/usr/bin/env python3

import torch
import torch.autograd as autograd
import random
import torch.nn as nn
import torch.optim as optim
import numpy

class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.lstm_0 = nn.LSTMCell(1, 64)
        self.lstm_1 = nn.LSTMCell(64, 64)
        self.dense_0 = nn.Linear(64, 1)

    
    def forward(self, inputs, forward=0):
        c_0 = autograd.Variable(torch.zeros(1, 64))
        h_0 = autograd.Variable(torch.zeros(1, 64))
        c_1 = autograd.Variable(torch.zeros(64, 64))
        h_1 = autograd.Variable(torch.zeros(64, 64))

        outputs = []
        for input_t in inputs.chunk(inputs.size(0), dim=1):
            h_0, c_0 = self.lstm_0(input_t, (h_0, c_0))
            h_1, c_1 = self.lstm_1(h_0, (h_1, c_1))
            output_t = self.dense_0(h_1.view(1, 64))
            outputs += [output_t]
        for foward in range(forward):
            h_0, c_0 = self.lstm_0(output_t, (h_0, c_0))
            h_1, c_1 = self.lstm_1(h_0, (h_1, c_1))
            output_t = self.dense_0(h_1.view(1, 64))
            outputs += [output_t]
        return torch.stack(outputs, 1).squeeze(2)


data = numpy.loadtxt('waves.txt')

model = Model()
criterion = nn.MSELoss()
#optimizer = optim.Adam(model.parameters(), lr=0.001)
optimizer = optim.LBFGS(model.parameters())
inputs = autograd.Variable(torch.from_numpy(data[series, :-1]))
targets = autograd.Variable(torch.from_numpy(data[series, 1:]))

for epoch in range(32):
    print('epoch: {0}'.format(epoch))
    def iteration():
        series = random.choice(range(100))
        outputs = model(inputs)
        loss = criterion(outputs, targets)
        print('\tloss: {0}'.format(loss.data.numpy()[0]))
        optimizer.zero_grad()
        loss.backward()
        return loss
    optimizer.step(iteration)


    import matplotlib.pyplot as pyplot
    import numpy

    window = 128
    inputs_test = autograd.Variable(torch.from_numpy(data[1, :-window]))
    predictions = model(inputs_test, forward=window)
    y = predictions.data.numpy()
    pyplot.plot(data[1, 1:])
    pyplot.plot(numpy.arange(inputs_test.size(0) + window), y.reshape(inputs_test.size(0) + window), 'r:')
    pyplot.savefig('predicted.png')
    pyplot.close()

