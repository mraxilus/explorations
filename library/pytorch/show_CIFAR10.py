#!/usr/bin/env python3

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot
import numpy
import load_CIFAR10
import torchvision


def show_image(image):
    image = image / 2 + 0.5
    numpy_image = image.numpy()
    pyplot.imshow(numpy.transpose(numpy_image, (1, 2, 0)))
    pyplot.show()


image_iterator = iter(load_CIFAR10.train_loader)
images, labels = image_iterator.next()


if __name__ == '__main__':
    show_image(torchvision.utils.make_grid(images))
    print(' '.join(load_CIFAR10.classes[labels[i]] for i in range(4)))
