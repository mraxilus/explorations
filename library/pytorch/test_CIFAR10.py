#!/usr/bin/env python3

import load_CIFAR10
import show_CIFAR10
import train_CIFAR10
from torch import nn, autograd, optim
import torch
import torchvision

test_iterator = iter(load_CIFAR10.test_loader)
images, labels = test_iterator.next()
show_CIFAR10.show_image(torchvision.utils.make_grid(images))
print('ground truth:', ' '.join(load_CIFAR10.classes[labels[i]] for i in range(4)))

outputs = train_CIFAR10.model(autograd.Variable(images))
confidences, predictions = torch.max(outputs.data, 1)

print('predictions:', ', '.join(load_CIFAR10.classes[labels[i]] + ' ' + str(confidences[i]) for i in range(4)))
