#!/usr/bin/env python3

import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self, channels=1):
        super(Net, self).__init__()
        self.channels = channels
        self.convolution_0 = nn.Conv2d(self.channels, 64, 5)
        self.convolution_1 = nn.Conv2d(64, 32, 5)
        self.fully_connected_0 = nn.Linear(32 * 5 * 5, 120)
        self.fully_connected_1 = nn.Linear(120, 84)
        self.fully_connected_2 = nn.Linear(84, 10)


    def forward(self, x):
        x = F.max_pool2d(F.relu(self.convolution_0(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.convolution_1(x)), (2, 2))
        x = x.view(-1, 32 * 5 * 5)
        x = F.relu(self.fully_connected_0(x))
        x = F.relu(self.fully_connected_1(x))
        x = self.fully_connected_2(x)
        return x


if __name__ == '__main__':
    net = Net(channels=3)
    print(net)
