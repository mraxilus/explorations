#!/usr/bin/env python3

import pandas
import rpy2.robjects as robjects

# Allow for automatic data frame conversion.
import rpy2.robjects.pandas2ri
robjects.pandas2ri.activate()


def load_functions(interface: rpy2.robjects.R, path: str):
    with open(path) as handle:
        interface(handle.read())


def main():
    load_functions(robjects.r, 'functions.r')
    change_dataframe = robjects.r['change_dataframe']
    dataframe = pandas.DataFrame({
        'a': [1, 3], 
        'change_me': [2, 4],
    })
    print(dataframe)
    print(change_dataframe(dataframe))


if __name__ == '__main__':
    main()


