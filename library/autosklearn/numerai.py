#!/usr/bin/env python3
#
# Provide an example of auto-sklearn usage via http://numer.ai/

import autosklearn.classification
import pandas
import numpy

# Load the training data.
training_data = pandas.read_csv('data/numerai_training_data.csv')
X = training_data.drop('target', 1)
y = training_data['target']

# Create and fit a classification ensemble automatically.
model = autosklearn.classification.AutoSklearnClassifier()
model.fit(numpy.array(X), numpy.array(y), metric='pac_metric')

# Load the tournament data.
tournament_data = pandas.read_csv('data/numerai_tournament_data.csv')
X_tournament = tournament_data.drop('t_id', 1)

# Predict the probability of each class for each element.
y_tournament = pandas.DataFrame(model.predict_proba(numpy.array(X_tournament)))

# Format the data correctly.
y_tournament['t_id'] = tournament_data['t_id']
y_tournament['probability'] = y_tournament[1]
y_tournament = y_tournament[['t_id', 'probability']]

# Save prediction results.
y_tournament.to_csv('data/predictions.csv', index=False)
