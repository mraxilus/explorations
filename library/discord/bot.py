#!/usr/bin/env python3

import discord
import discord.ext.commands

bot = discord.ext.commands.Bot(command_prefix='!') 


@bot.event
async def on_ready():
    print('login as {0.name} with id set to {0.id}'.format(bot.user))


@bot.command()
async def joined(member : discord.Member):
    """Says when a member joined."""
    await bot.say('{0.name} joined in {0.joined_at}'.format(member))


@bot.command(pass_context=True, no_pm=True)
async def whoami(context):
    """Provide details about the user who's asking."""
    author = context.message.author
    await bot.say('you are {0.name}, a server {0.top_role}'.format(author))

bot.run('TOKEN')
