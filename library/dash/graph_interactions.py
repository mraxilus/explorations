#!/usr/bin/env python3

import dash
import dash.dependencies as dependency
import dash_core_components as core
import dash_html_components as html
import yaml

APP = dash.Dash(__name__)

STYLES = {
    'column': {
        'display': 'inline-block',
        'verticalAlign': 'text-top',
        'width': '33%',
        'padding': 10,
        'boxSizing': 'border-box',
        'minHeight': '200px',
    },
    'pre': {
        'border': 'thin lightgrey solid',
        'padding': '10px',
    },
}

APP.layout = html.Div([
    core.Graph(
        id='figure',
        figure={
            'data': [
                {
                    'x': [1, 2, 3, 4],
                    'y': [4, 1, 3, 5],
                    'text': ['a', 'b', 'c', 'd'],
                    'customdata': ['c.a', 'c.b', 'c.c', 'c.d'],
                    'name': 'Trace 0',
                    'mode': 'markers',
                    'marker': {'size': 12},
                },
                {
                    'x': [1, 2, 3, 4],
                    'y': [9, 4, 1, 4],
                    'text': ['w', 'x', 'y', 'z'],
                    'customdata': ['c.w', 'c.x', 'c.y', 'c.z'],
                    'name': 'Trace 1',
                    'mode': 'markers',
                    'marker': {'size': 12},
                },
            ],
        }
    ),
    html.Div([
        core.Markdown('''
        ## Hover Data
        Mouse over values in the graph.
        '''.replace('  ', '')),
        html.Pre(id='hover-data', style=STYLES['pre']),
    ], style=STYLES['column']),
    html.Div([
        core.Markdown('''
        ## Click Data
        Click on the points in the graph.
        '''.replace('  ', '')),
        html.Pre(id='click-data', style=STYLES['pre']),
    ], style=STYLES['column']),
    html.Div([
        core.Markdown('''
        ## Selection Data
        Select points in the graph with either the box or lasso tools.
        '''.replace('  ', '')),
        html.Pre(id='selection-data', style=STYLES['pre']),
    ], style=STYLES['column']),
])


@APP.callback(
    dependency.Output('hover-data', 'children'),
    [dependency.Input('figure', 'hoverData')],
)
def show_hover_data(data):
    return yaml.dump(data)


@APP.callback(
    dependency.Output('click-data', 'children'),
    [dependency.Input('figure', 'clickData')],
)
def show_click_data(data):
    return yaml.dump(data)


@APP.callback(
    dependency.Output('selection-data', 'children'),
    [dependency.Input('figure', 'selectedData')],
)
def show_selection_data(data):
    return yaml.dump(data)


if __name__ == '__main__':
    APP.run_server(debug=True)

