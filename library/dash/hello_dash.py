#!/usr/bin/env python3
import dash
import dash_core_components as core
import dash_html_components as html

app = dash.Dash()

app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),
    html.Div(children='''
        Dash is an interactive web application framework for Python.
    '''),
    core.Graph(
        id='hello-dash-graph',
        figure={
            'data': [
                {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'London'},
                {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': 'Bristol'},
            ],
            'layout': {
                'title': 'Dash Data Visualisation',
            }
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)
