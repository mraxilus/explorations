#!/usr/bin/env python3

import dash
import dash.dependencies as dependency
import dash_core_components as core
import dash_html_components as html

APP = dash.Dash()

COUNTRIES = ['Bermuda', 'United Kingdom', 'United States']
CITIES = ['Hamilton', 'London', 'New York']

APP.layout = html.Div([
    core.RadioItems(
        id='country-selector',
        options=[{'label': country, 'value': country} for country in COUNTRIES],
        value=COUNTRIES[0]
    ),
    html.Div(id='selected-country'),
    core.RadioItems(
        id='city-selector',
        options=[{'label': city, 'value': city} for city in CITIES],
        value=CITIES[0],
    ),
    html.Div(id='selected-city'),
])
    

@APP.callback(
    dependency.Output('selected-country', 'children'),
    [dependency.Input('country-selector', 'value')]
)
def update_selected_country(country):
    return 'You selected the country: {}'.format(country)


@APP.callback(
    dependency.Output('selected-city', 'children'),
    [dependency.Input('city-selector', 'value')]
)
def update_selected_city(city):
    return 'You selected the country: {}'.format(city)


if __name__ == '__main__':
    APP.run_server(debug=True)

