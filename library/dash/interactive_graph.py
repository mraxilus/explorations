#!/usr/bin/env python3

import dash
import dash.dependencies as dependencies
import dash_core_components as core
import dash_html_components as html
import pandas
import plotly.graph_objs as graphs

countries = pandas.read_csv('https://raw.githubusercontent.com/plotly/'
                            'datasets/master/gapminderDataFiveYear.csv')

app = dash.Dash()

app.layout = html.Div([
    core.Graph(id='graph', animate=True),
    core.Slider(
        id='year',
        min=countries['year'].min(),
        max=countries['year'].max(),
        value=countries['year'].min(),
        step=None,
        marks={str(year): str(year) for year in countries['year'].unique()}
    )
])


@app.callback(
    dependencies.Output('graph', 'figure'),
    [dependencies.Input('year', 'value')],
)
def update_countries(selected_year):
    countries_by_year = countries[countries.year == selected_year]

    traces = []
    for continent in countries_by_year.continent.unique():
        countries_by_continent = countries_by_year[countries_by_year['continent'] == continent]
        traces.append(graphs.Scatter(
            x=countries_by_continent['gdpPercap'],
            y=countries_by_continent['lifeExp'],
            text=countries_by_continent['country'],
            mode='markers',
            opacity=0.7,
            marker={
                'size': 15,
                'line': {'width': 0.5, 'color': 'white'}
            },
            name=continent
        ))

    return {
        'data': traces,
        'layout': graphs.Layout(
            xaxis={'type': 'log', 'title': 'GDP Per Capita'},
            yaxis={'title': 'Life Expectancy', 'range': [20, 90]},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            legend={'x': 0, 'y': 1},
            hovermode='closest'
        )
    }

if __name__ == '__main__':
    app.run_server(debug=True)
