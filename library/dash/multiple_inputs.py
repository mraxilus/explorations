#!/usr/bin/env python3

import dash
import dash.dependencies as dependency
import dash_core_components as core
import dash_html_components as html
import pandas
import plotly.graph_objs as graph

APP = dash.Dash()

COUNTRIES = pandas.read_csv('https://gist.githubusercontent.com/chriddyp/'
                            'cb5392c35661370d95f300086accea51/raw/'
                            '8e0768211f6b747c0db42a9ce9a0937dafcbd8b2/'
                            'indicators.csv')
COUNTRY_INDICATORS = COUNTRIES['Indicator Name'].unique()

APP.layout = html.Div([
    html.Div([
        html.Div([
            core.Dropdown(
                id='x-axis-indicator',
                options=[{'label': indicator, 'value': indicator} for indicator in COUNTRY_INDICATORS],
                value=COUNTRY_INDICATORS[0],
            ),
            core.RadioItems(
                id='x-axis-scale',
                options=[{'label': scale, 'value': scale} for scale in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'},
            ),
        ], style={'width': '48%', 'display': 'inline-block'}),
        html.Div([
            core.Dropdown(
                id='y-axis-indicator',
                options=[{'label': indicator, 'value': indicator} for indicator in COUNTRY_INDICATORS],
                value=COUNTRY_INDICATORS[1],
            ),
            core.RadioItems(
                id='y-axis-scale',
                options=[{'label': scale, 'value': scale} for scale in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'},
            ),
        ], style={'width': '48%', 'display': 'inline-block'}),
    ]),
    core.Graph(id='scatter-plot'),
    core.Slider(
        id='year-slider',
        min=COUNTRIES['Year'].min(),
        max=COUNTRIES['Year'].max(),
        value=COUNTRIES['Year'].max(),
        step=None,
        marks={str(year): str(year) for year in COUNTRIES['Year'].unique()},
    ),
], style={'margin': '10px'})


@APP.callback(
    dependency.Output('scatter-plot', 'figure'),
    [
        dependency.Input('x-axis-indicator', 'value'),
        dependency.Input('x-axis-scale', 'value'),
        dependency.Input('y-axis-indicator', 'value'),
        dependency.Input('y-axis-scale', 'value'),
        dependency.Input('year-slider', 'value'),
    ]
)
def update_figure(x_indicator, x_scale, y_indicator, y_scale, year_selected):
    countries = COUNTRIES[COUNTRIES['Year'] == year_selected]

    return {
        'data': [graph.Scatter(
            x=countries[countries['Indicator Name'] == x_indicator]['Value'],
            y=countries[countries['Indicator Name'] == y_indicator]['Value'],
            text=countries[countries['Indicator Name'] == y_indicator]['Country Name'],
            mode='markers',
            marker={
                'size': 15,
                'opacity': 0.5,
                'line': {'width': 0.5, 'color': 'white'},
            },
        )],
        'layout': graph.Layout(
            xaxis={
                'title': x_indicator,
                'type': x_scale.lower() if x_scale else 'linear',
            },
            yaxis={
                'title': y_indicator,
                'type': y_scale.lower() if y_scale else 'linear',
            },
            margin={'l': 40, 'b': 40, 't': 10, 'r': 0},
            hovermode='closest',
        ),
    }

    
if __name__ == '__main__':
    APP.run_server(debug=True)

