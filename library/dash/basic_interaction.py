#!/usr/bin/env python3

import dash
import dash.dependencies as dependencies
import dash_core_components as core
import dash_html_components as html

app = dash.Dash()

app.layout = html.Div([
    core.Input(id='text-input', value='nothing', type='text'),
    html.Div(id='text-output')
])


@app.callback(
    dependencies.Output(component_id='text-output', component_property='children'),
    [dependencies.Input(component_id='text-input', component_property='value')]
)
def update_text(input_value):
    return 'In the field above you typed "{}"'.format(input_value)


if __name__ == '__main__':
    app.run_server(debug=True)
