#!/usr/bin/env python3

import dash
import dash.dependencies as dependency
import dash_core_components as core
import dash_html_components as html

APP = dash.Dash()

COUNTRIES = {
    'Bermuda': ['Hamilton', 'Smiths', 'Southampton'],
    'United Kingdom': ['Bristol', 'London', 'Reading'],
}

APP.layout = html.Div([
    core.RadioItems(
        id='country-selector',
        options=[{'label': country, 'value': country} for country in COUNTRIES.keys()],
        value=list(COUNTRIES.keys())[0],
    ),
    html.Hr(),
    core.RadioItems(id='city-selector'),
    html.Hr(),
    html.Div(id='selected-location'),
])


@APP.callback(
    dependency.Output('city-selector', 'options'),
    [dependency.Input('country-selector', 'value')],
)
def set_available_cities(country):
    return [{'label': city, 'value': city} for city in COUNTRIES[country]]


@APP.callback(
    dependency.Output('city-selector', 'value'),
    [dependency.Input('city-selector', 'options')],
)
def set_default_city(available_cities):
    return available_cities[0]['value']


@APP.callback(
    dependency.Output('selected-location', 'children'),
    [
        dependency.Input('country-selector', 'value'),
        dependency.Input('city-selector', 'value'),
    ]
)
def display_chosen_location(country, city):
    return 'You are in {1}, {0}.'.format(country, city)



if __name__ == '__main__':
    APP.run_server(debug=True)

