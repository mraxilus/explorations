#!/usr/bin/env python3
#
# OpenCV
# ======

# OpenCV for Python is actually a wrapper for the OpenCV C++ library.
# This wrapper can be imported from the `cv2` module.
# Annoyingly, even OpenCV 3.0 uses `cv2` rather than `cv3`, or simply, `cv`.
import cv2

# In this program we are going to perform a threshold operation on an image.
# This will demonstrate the basic use of this image processing library.
# To do so we must create a gray scale matrix representation of the image.
#
# <img src="../image/dog.jpg" width="100%"/>
image = cv2.imread('image/dog.jpg', cv2.IMREAD_GRAYSCALE)

# We then apply a binary threshold against the matrix.
# In this case any pixel with an intensity above 127 will be maximised.
# Conversely, any pixel with an intensity below 127 will be minimized.
#
# <img src="../image/dog_binary.jpg" width="100%"/>
image_binary = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)[1]

# Finally, we output a new image which is the result of the transformation.
cv2.imwrite('image/dog_binary.jpg', image_binary)

