#!/bin/bash

nvidia-docker run --interactive --tty --volume $(pwd):/tensorflow/source --publish 8888:8888 tensorflow console
