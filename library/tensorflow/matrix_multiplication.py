#!/usr/bin/env python3
#
# To run this file, first run ./run.sh then run this command:
#   %run source/matrix_multiplication.py

import tensorflow

# Setup graph.
matrix_1 = tensorflow.constant([
  [2.,3.],
])
matrix_2 = tensorflow.constant([
  [4.],
  [5.],
])
product = tensorflow.matmul(matrix_1, matrix_2)

# Execute graph.
with tensorflow.Session() as session:
    result = session.run(product)
    print(result)
