#!/bin/bash
#
# see http://askubuntu.com/a/244642/25522 for details.

# perform setup
mkdir virtualenvs
export WORKON_HOME=$(pwd)/virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

# create virtualenv
mkvirtualenv hello_virtualenv
python -c "import sys; print sys.path"
deactivate

# reconnect
workon hello_virtualenv
python -c "import sys; print sys.path"
deactivate

# clean up
rmvirtualenv hello_virtualenv
rm -r virtualenvs
