#! /usr/bin/env python3

import random
import unittest

class TestSequenceFunctions(unittest.TestCase):
  def setUp(self):
    self.sequence = list(range(10))

  def test_shuffle(self):
    random.shuffle(self.sequence)
    self.sequence.sort()
    self.assertEqual(self.sequence, list(range(10)))
    self.assertRaises(TypeError, random.shuffle, (1, 2, 3))

  def test_choice(self):
    element = random.choice(self.sequence)
    self.assertTrue(element in self.sequence)

  def test_sample(self):
    with self.assertRaises(ValueError):
      random.sample(self.sequence, 20)
    for element in random.sample(self.sequence, 5):
      self.assertTrue(element in self.sequence)

if __name__ == '__main__':
  suite = unittest.TestLoader().loadTestsFromTestCase(TestSequenceFunctions)
  unittest.TextTestRunner(verbosity=2).run(suite)
