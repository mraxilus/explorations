#!/usr/bin/env python3

import blobconverter as bc
import cv2 as cv
import depthai as dai
import numpy as np

STREAM_NAME_CAMERA = "camera"
STREAM_NAME_MODEL = "model"

def main():
    """Connect to OAK-D, detect objects, and display results."""
    # Setup pipeline.
    pipeline = dai.Pipeline()

    # Setup camera node.
    camera = pipeline.createColorCamera()
    camera.setPreviewSize(300, 300)  # Match model input shape.
    camera.setInterleaved(False)

    # Setup neural network model node.
    model = pipeline.createMobileNetDetectionNetwork()
    model.setBlobPath(bc.from_zoo(name='mobilenet-ssd', shaves=6))
    model.setConfidenceThreshold(0.5)
    camera.preview.link(model.input)  # Provide camera preview as model input.

    # Setup image preview retrieval.
    output_camera = pipeline.createXLinkOut()
    output_camera.setStreamName(STREAM_NAME_CAMERA)
    camera.preview.link(output_camera.input)

    # Setup model prediction retrieval.
    output_model = pipeline.createXLinkOut()
    output_model.setStreamName(STREAM_NAME_MODEL)
    model.out.link(output_model.input)

    print("Finished pipeline configuration.")

    # Connect to device and grab frames/predictions.
    with dai.Device(pipeline) as device:
        print("Connected to device.")

        queue_camera = device.getOutputQueue(STREAM_NAME_CAMERA)
        queue_model = device.getOutputQueue(STREAM_NAME_MODEL)

        print("Setup frame queues.")

        frame = None
        detections = []
        while True:
            item_camera = queue_camera.tryGet()
            item_model = queue_model.tryGet()

            if item_camera is not None:
                frame = item_camera.getCvFrame()

            if item_model is not None:
                detections = item_model.detections

            if frame is not None:
                # Draw bouding box for each detection.
                for d in detections:
                    bounding_box = to_frame_coordinates(
                        (d.xmin, d.ymin, d.xmax, d.ymax),
                        frame=frame,
                    )
                    cv.rectangle(
                        frame,
                        pt1=(bounding_box[0], bounding_box[1]),
                        pt2=(bounding_box[2], bounding_box[3]),
                        color=(255, 0, 0),
                        thickness=2,
                    )

                cv.imshow("preview", frame)
                print(".")

            if cv.waitKey(1) == ord('q'):
                break
                        

def to_frame_coordinates(
        bounding_box: np.array,
        *,
        frame: np.array
) -> np.array:
    """Convert normalized bounding_box coordinates to frame coordinates."""
    values = np.full(len(bounding_box), frame.shape[0])
    values[::2] = frame.shape[1]
    return (np.clip(np.array(bounding_box), 0, 1) * values).astype(int)


if __name__ == '__main__':
    main()
