#!/usr/bin/env python3

import cv2 as cv
import depthai as dai

STREAM_NAME = "camera"
ADDRESS = "192.168.1.94"

def main():
  """Connect to specific OAK-D POE and display camera preview."""
  # Setup pipeline.
  pipeline = dai.Pipeline()
  camera = pipeline.createColorCamera()
  output = pipeline.createXLinkOut()
  output.setStreamName(STREAM_NAME)
  camera.preview.link(output.input)

  # Select specific OAK-D by IP.
  device_info = dai.DeviceInfo()
  device_info.state = dai.XLinkDeviceState.X_LINK_BOOTLOADER
  device_info.desc.protocol = dai.XLinkProtocol.X_LINK_TCP_IP
  device_info.desc.name = ADDRESS 

  # Connect to device and grab frames.
  with dai.Device(pipeline) as device:
    queue = device.getOutputQueue(
      name=STREAM_NAME,
      maxSize=4,
      blocking=False,
    )

    # Display camera frames.
    while True:
      cv.imshow("preview", queue.get().getCvFrame())
      if cv.waitKey(1) == ord('q'):
        break

if __name__ == '__main__':
  main()
