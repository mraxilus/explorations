#!/usr/bin/env python3

import gtts

text = 'The beige hue on the waters of the loch impressed all, including the French queen, before she heard that symphony again, just as young Arthur wanted.'
synthesizer = gtts.gTTS(text=text, lang='en')
synthesizer.save('phonetic_pangram.mp3')
